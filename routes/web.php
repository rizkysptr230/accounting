<?php

use Illuminate\Support\Facades\Route;

Route::get('/token', function () {
    return csrf_token();
});
Route::get('/', 'AuthController@showFormLogin')->name('login');
Route::get('login', 'AuthController@showFormLogin')->name('login');
Route::post('login', 'AuthController@login');

Route::group(['middleware' => 'auth'], function () {

    Route::get('home', 'HomeController@index')->name('home');
    Route::get('logout', 'AuthController@logout')->name('logout');

    Route::get('/coa',[
        'uses' => 'login\coa@index',
        'as' => 'login.coa'
    ]);
    Route::get('/coa/{id}',[
        'uses' => 'login\coa@getCoa',
        'as' => 'login.getcoa'
    ]);
    Route::post('/coa/put',[
        'uses' => 'login\coa@putCoa',
        'as' => 'login.putcoa'
    ]);
    Route::get('/newcoa/',[
        'uses' => 'login\coa@newCoa',
        'as' => 'login.newcoa'
    ]);
    Route::post('/newcoa',[
        'uses' => 'login\coa@postCoa',
        'as' => 'login.postcoa'
    ]);

    // report
    Route::get('/trial-balance', 'TrialBalanceController@index');
    Route::get('/trial-balance-tables', 'TrialBalanceTableController@index');

    Route::get('/neraca', 'NeracaController@index');
    Route::get('/neraca-tables', 'NeracaTableController@index');

    Route::get('/profit', 'ProfitController@index');
    Route::get('/profit-tables', 'ProfitTableController@index');

    // journal manual
    Route::get('/manual-journals', 'ManualJournalController@index');
    Route::post('/manual-journals', 'ManualJournalController@store');
    Route::delete('/manual-journals/{noBatch}', 'ManualJournalController@destroy');
    Route::get('/manual-journal-tables', 'ManualJournalTableController@index');

    Route::get('/manual-journals/{noBatch}/details', 'ManualJournalDetailController@index');
    Route::post('/manual-journals/{noBatch}/details', 'ManualJournalDetailController@store');
    Route::put('/manual-journals/{noBatch}', 'ManualJournalController@update');
    Route::delete('/manual-journal-details/{id}', 'ManualJournalDetailController@destroy');

    Route::get('/posting-journals', 'PostingJournalController@index');
    Route::get('/posting-journals/{noBatch}/details', 'PostingJournalDetailController@index');

    //API
    Route::get('/api/dttran', 'ApiController@dttran');
    Route::get('/api/smtran', 'ApiController@smtran');

    Route::get('/api/login', 'ApiLoginController@token');

});
