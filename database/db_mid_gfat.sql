-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 02, 2021 at 04:40 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mid_gfat`
--

-- --------------------------------------------------------

--
-- Table structure for table `dt_tran`
--

CREATE TABLE `dt_tran` (
  `id` int(11) NOT NULL,
  `noBatch` int(11) NOT NULL,
  `prdbln` int(11) NOT NULL,
  `prdthn` int(11) NOT NULL,
  `nourut` int(11) NOT NULL,
  `kodeCoa` varchar(6) NOT NULL,
  `keterangan` text NOT NULL,
  `dk` varchar(1) NOT NULL,
  `amount` double NOT NULL,
  `tgl_proses` date NOT NULL,
  `tgl_validasi` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dt_tran`
--

INSERT INTO `dt_tran` (`id`, `noBatch`, `prdbln`, `prdthn`, `nourut`, `kodeCoa`, `keterangan`, `dk`, `amount`, `tgl_proses`, `tgl_validasi`) VALUES
(1, 2, 2, 2018, 4, '100000', 'tess', 'D', 1000000, '2020-12-07', '2020-12-07'),
(2, 2, 2, 2018, 4, '100000', 'tess', 'K', -1000000, '2020-12-07', '2020-12-07'),
(3, 3, 2, 2018, 4, '100000', 'yes', 'D', 100000000.123123, '2020-12-07', '2020-12-10'),
(4, 4, 2, 2018, 4, '200000', 'qwerty', 'D', 100000, '2020-12-07', '2021-01-13'),
(5, 4, 2, 2018, 4, '100000', 'qwerty', 'K', -100000, '2020-12-07', '2021-01-13'),
(6, 9, 2, 2018, 4, '100000', 'test', 'D', 10000000, '2021-01-14', '2021-01-14'),
(7, 9, 2, 2018, 4, '100000', 'test2', 'D', 10000000, '2021-01-14', '2021-01-14'),
(8, 9, 2, 2018, 4, '100000', 'test3', 'K', -20000000, '2021-01-14', '2021-01-14'),
(9, 6, 2, 2018, 4, '500000', 'test', 'D', 500000, '2021-01-14', '2021-01-15'),
(11, 6, 2, 2018, 4, '100000', 'test', 'K', -800000, '2021-01-14', '2021-01-15'),
(12, 6, 2, 2018, 4, '100000', 'test', 'K', -200000, '2021-01-14', '2021-01-15'),
(14, 10, 2, 2018, 4, '111101', 'test', 'D', 1000000, '2021-01-15', '2021-01-18'),
(15, 13, 2, 2018, 0, '512001', 'test', 'D', 1000000, '2021-01-26', '2021-01-26'),
(16, 13, 2, 2018, 0, '512001', 'test2', 'K', -1000000, '2021-01-26', '2021-01-26'),
(17, 16, 4, 2021, 0, '411001', 'test', 'D', 100000000, '2021-01-27', '2021-01-27'),
(19, 17, 1, 2021, 0, '411001', 'test', 'D', 100000000, '2021-01-28', '2021-01-28'),
(20, 17, 1, 2021, 0, '451001', 'test2', 'D', 20000000, '2021-01-28', '2021-01-28'),
(21, 17, 1, 2021, 0, '451999', 'test', 'D', 10000000, '2021-01-28', '2021-01-28'),
(22, 18, 1, 2021, 0, '115112', 'test', 'K', -123000000, '2021-01-28', '2021-01-28'),
(23, 18, 1, 2021, 0, '621001', 'test', 'D', 100000000, '2021-01-28', '2021-01-28'),
(24, 18, 1, 2021, 0, '621999', 'test', 'D', 12000000, '2021-01-28', '2021-01-28'),
(25, 19, 1, 2021, 0, '512002', 'test', 'K', -20000000, '2021-01-28', '2021-01-28'),
(26, 19, 1, 2021, 0, '621017', 'test', 'D', 120000000, '2021-01-28', '2021-01-28');

-- --------------------------------------------------------

--
-- Table structure for table `neraca_group`
--

CREATE TABLE `neraca_group` (
  `idGroup` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `parent_group_id` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `neraca_group`
--

INSERT INTO `neraca_group` (`idGroup`, `name`, `parent_group_id`, `is_active`, `created_at`, `updated_at`) VALUES
(10, 'Kas & Setara Kas', 3, 1, NULL, NULL),
(11, 'Piutang Usaha Fintech', 3, 1, NULL, NULL),
(12, 'Biaya Dibayar Dimuka', 3, 1, NULL, NULL),
(13, 'Aset Tetap', 3, 1, NULL, NULL),
(14, 'Aset Tidak Berwujud', 3, 1, NULL, NULL),
(15, 'Aset Lainnya', 3, 1, NULL, NULL),
(16, 'Utang Pihak Berelasi', 4, 1, NULL, NULL),
(17, 'Utang Pajak', 4, 1, NULL, NULL),
(18, 'Cadangan THR & Bonus', 4, 1, NULL, NULL),
(19, 'Utang Lainnya', 4, 1, NULL, NULL),
(20, 'Cadangan Imbalan Pasca Kerja', 4, 1, NULL, NULL),
(21, 'Ekuitas', 5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `parent_group`
--

CREATE TABLE `parent_group` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parent_group`
--

INSERT INTO `parent_group` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Pendapatan', 1, NULL, NULL),
(2, 'Biaya', 1, NULL, NULL),
(3, 'Aktiva', 1, NULL, NULL),
(4, 'Liabilitas', 1, NULL, NULL),
(5, 'Ekuitas', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profit_group`
--

CREATE TABLE `profit_group` (
  `idGroup` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `parent_profit_group_id` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profit_group`
--

INSERT INTO `profit_group` (`idGroup`, `name`, `parent_profit_group_id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Pendapatan', 2, 1, NULL, NULL),
(2, 'Biaya Keuangan', 2, 1, NULL, NULL),
(3, 'Biaya Marketing', 2, 1, NULL, NULL),
(4, 'Biaya Ketenagakerjaan', 2, 1, NULL, NULL),
(5, 'Biaya Operasional', 2, 1, NULL, NULL),
(6, 'Biaya Pemeliharaan', 2, 1, NULL, NULL),
(7, 'Biaya Penyusutan Aktiva Tetap', 2, 1, NULL, NULL),
(8, 'Biaya Penyusutan Aset Tidak Berwujud', 2, 1, NULL, NULL),
(9, 'Biaya Lainnya', 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `siskon`
--

CREATE TABLE `siskon` (
  `id` int(11) NOT NULL,
  `prdbln` int(11) NOT NULL,
  `prdthn` int(11) NOT NULL,
  `tgl_siskon` date NOT NULL,
  `tgl_proses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siskon`
--

INSERT INTO `siskon` (`id`, `prdbln`, `prdthn`, `tgl_siskon`, `tgl_proses`) VALUES
(1, 2, 2018, '2018-02-01', '2019-09-06 09:02:55');

-- --------------------------------------------------------

--
-- Table structure for table `sm_tran`
--

CREATE TABLE `sm_tran` (
  `noBatch` int(11) NOT NULL,
  `prdbln` int(11) NOT NULL,
  `prdthn` int(11) NOT NULL,
  `jurName` varchar(45) NOT NULL,
  `keterangan` text NOT NULL,
  `amount` double NOT NULL,
  `isPost` int(11) NOT NULL,
  `tgl_proses` date NOT NULL,
  `userId` varchar(25) NOT NULL,
  `tgl_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sm_tran`
--

INSERT INTO `sm_tran` (`noBatch`, `prdbln`, `prdthn`, `jurName`, `keterangan`, `amount`, `isPost`, `tgl_proses`, `userId`, `tgl_update`) VALUES
(1, 2, 2018, 'Pembayaran', 'jhaaa', 20000000, 1, '2020-12-04', '1', '2020-12-04 12:45:01'),
(2, 2, 2018, 'Pelunasan', 'tess', 1000000, 1, '2020-12-07', '1', '2020-12-07 03:11:57'),
(4, 2, 2018, 'pelunasan', 'yayaya', 1000000, 1, '2020-12-07', '1', '2020-12-07 07:30:04'),
(6, 2, 2018, 'rizky', 'dwadq', 1000000, 1, '2021-01-14', '1', '2021-01-14 07:33:54'),
(9, 2, 2018, 'rizky', 'test', 20000000, 1, '2021-01-14', '1', '2021-01-14 16:34:19'),
(10, 2, 2018, 'rizky', '13124', 10000000, 0, '2021-01-15', '1', '2021-01-15 08:58:45'),
(11, 2, 2018, '3131', 'dadad', 100, 0, '2021-01-18', '1', '2021-01-18 07:14:22'),
(12, 2, 2018, 'rizky', '10321', 1000000, 0, '2021-01-18', '1', '2021-01-18 07:15:59'),
(13, 2, 2018, 'Biaya', 'Pengeluaran Hari Ini', 10000000, 1, '2021-01-26', '1', '2021-01-26 07:04:15'),
(15, 2, 2021, 'test', 'aa', 20000000, 0, '2021-01-27', '1', '2021-01-27 08:42:45'),
(16, 4, 2021, 'testperiode', 'testperiode', 12300000, 0, '2021-01-27', '1', '2021-01-27 08:45:55'),
(17, 1, 2021, 'Pendapatan', 'test', 120000000, 0, '2021-01-28', '1', '2021-01-28 06:25:59'),
(18, 1, 2021, 'Gaji', 'test', 500000000, 0, '2021-01-28', '1', '2021-01-28 06:28:58'),
(19, 1, 2021, 'Biaya', 'test', 100000000, 0, '2021-01-28', '1', '2021-01-28 06:32:35');

-- --------------------------------------------------------

--
-- Table structure for table `tblcoa`
--

CREATE TABLE `tblcoa` (
  `kodeCoa` varchar(6) NOT NULL DEFAULT '0',
  `nourut` int(11) NOT NULL DEFAULT '0',
  `keterangan` varchar(45) NOT NULL DEFAULT '0',
  `isActive` int(11) NOT NULL DEFAULT '0',
  `tgl_proses` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` varchar(25) DEFAULT '0',
  `idGroup` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcoa`
--

INSERT INTO `tblcoa` (`kodeCoa`, `nourut`, `keterangan`, `isActive`, `tgl_proses`, `userId`, `idGroup`, `created_at`, `updated_at`) VALUES
('100000', 4, 'AKTIVA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('110000', 3, 'AKTIVA LANCAR', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111000', 2, 'KAS DAN SETARA KAS', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111100', 1, 'KAS', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111101', 0, 'KAS BESAR', 1, '0000-00-00 00:00:00', '', 10, NULL, NULL),
('111200', 1, 'BANK INCOMING', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111201', 0, 'BANK PERMATA IN - HO', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111202', 0, 'BCA OPERASIONAL', 1, '0000-00-00 00:00:00', '', 10, NULL, NULL),
('111203', 0, 'BCA VIRTUAL ACCOUNT', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111204', 0, 'PERMATA VIRTUAL ACCOUNT', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111205', 0, 'BCA Escrow', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111206', 0, 'BNI VIRTUAL ACCOUNT', 1, '2020-01-21 14:37:10', NULL, 10, NULL, NULL),
('111207', 0, 'BNI ESCROW', 1, '2020-01-21 14:37:42', NULL, 10, NULL, NULL),
('111208', 0, 'BNI OPERASIONAL', 1, '2020-06-04 12:06:28', NULL, 10, NULL, NULL),
('111209', 0, 'BANK MNC', 1, '2021-01-28 09:57:01', '', 10, NULL, NULL),
('111210', 0, 'BANK MEGA', 1, '2021-01-28 09:57:51', '0', 10, NULL, NULL),
('111300', 1, 'BANK OUTGOING', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111301', 0, 'BANK PERMATA OUT - HO', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111302', 0, 'BANK BCA OUT - HO', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111600', 1, 'DEPOSITO', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111601', 0, 'DEPOSITO - BANK AMAR INDONESIA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('111602', 0, 'DEPOSITO - BNI', 1, '0000-00-00 00:00:00', '', 10, NULL, NULL),
('111603', 0, 'DEPOSITO - BANK VICTORIA SYARIAH', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('113000', 2, 'PIUTANG', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('113100', 1, 'PIUTANG USAHA INVESTASI', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('113101', 0, 'PIUTANG USAHA INVESTASI PT MAS', 1, '0000-00-00 00:00:00', '', 11, NULL, NULL),
('113102', 0, 'PIUTANG USAHA INVESTASI KSP MAS', 1, '0000-00-00 00:00:00', '', 11, NULL, NULL),
('113900', 1, 'PIUTANG LAIN-LAIN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('113901', 0, 'PIUTANG KARYAWAN', 1, '0000-00-00 00:00:00', '', 11, NULL, NULL),
('113902', 0, 'PIUTANG LAIN-LAIN', 1, '2020-02-14 15:33:50', NULL, 11, NULL, NULL),
('113999', 0, 'TL - LAINNYA', 1, '0000-00-00 00:00:00', '', 11, NULL, NULL),
('115000', 2, 'BIAYA DIBAYAR DIMUKA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('115100', 1, 'BIAYA DIBAYAR DIMUKA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('115101', 0, 'BDD - PERJALANAN DINAS', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115102', 0, 'BDD - Asuransi Kendaraan', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115103', 0, 'BDD - Asuransi Kesehatan', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115104', 0, 'BDD - Sewa Gedung / Kantor', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115105', 0, 'BDD - Sewa Kendaraan', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115106', 0, 'BDD - Sewa Inventaris Kantor', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115107', 0, 'BDD - Renovasi Gedung / Kantor', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115108', 0, 'BDD - SEWA RUMAH', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115109', 0, 'BDD - JASA PROFESIONAL', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115110', 0, 'BDD - IKLAN DAN PROMOSI', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115111', 0, 'BDD - CETAKAN', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115112', 0, 'BDD - GAJI KARYAWAN', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115113', 0, 'BDD - PROVISI', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115114', 0, 'BDD - THR & BONUS', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115115', 0, 'BDD - KEANGGOTAAN FINTECH', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115116', 0, 'BDD - PEMELIHARAAN SOFTWARE', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115199', 0, 'BDD - LAINNYA', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115200', 1, 'PAJAK DIBAYAR DIMUKA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('115201', 0, 'BDD - PAJAK PPH 25', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115202', 0, 'BDD - PAJAK PP46', 1, '0000-00-00 00:00:00', '', 12, NULL, NULL),
('115203', 0, 'BDD PPH 21', 1, '2019-04-09 17:43:20', NULL, 12, NULL, NULL),
('118000', 2, 'ASET KEUANGAN LANCAR LAINNYA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('118100', 1, 'DEPOSIT', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('118101', 0, 'DEPOSIT SEWA GEDUNG', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('118102', 0, '   DEPOSIT SW MESIN FOTO COPY', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('118103', 0, '   DEPOSIT PROVIDER INTERNET', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('118200', 1, 'OPEN ITEM', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('118201', 0, 'OPEN ITEM', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('130000', 3, 'AKTIVA TIDAK LANCAR', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('131000', 2, 'AKTIVA TETAP', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('131100', 1, 'NILAI PEROLEHAN AKTIVA TETAP', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('131101', 0, '1A-MEBEL DAN PERALATAN KAYU', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131102', 0, '1B-MESIN KANTOR(KOMP,PRINTER,SCANNER DLL)', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131103', 0, '1C-KAMERA DAN VIDEO', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131104', 0, '1D-SEPEDA MOTOR', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131105', 0, '1E-ALAT PERLENGKAPAN KHUSUS', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131106', 0, '1F-ALAT DAPUR', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131111', 0, '2A-MEBEL DAN PERALATAN LOGAM', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131112', 0, '2B-AC DAN KIPAS ANGIN', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131113', 0, '2C-KOMPUTER & PRINTER', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131114', 0, '2D-MOBIL, BUS & TRUCK', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131121', 0, 'TANAH', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131122', 0, 'BANGUNAN', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131200', 1, 'AKUMULASI PENYUSUTAN AKTIVA TETAP', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('131201', 0, 'AK. 1A-MEBEL & PERALATAN KAYU', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131202', 0, 'AK.1B-MESIN KANTOR(KOMP,PRINTER,SCANNER DLL)', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131203', 0, 'AK.1C-KAMERA & VIDEO', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131204', 0, 'AK.1D-SEPEDA MOTOR', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131205', 0, 'AK.1E-ALAT PERLENGKAPAN KHUSUS', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131206', 0, 'AK.1F-ALAT DAPUR', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131211', 0, 'AK.2A-MEBEL & PERALATAN LOGAM', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131212', 0, 'AK.2B-AC & KIPAS ANGIN', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131213', 0, 'AK.2C - KOMPUTER DAN PRINTER', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131214', 0, 'AK.2D-MOBIL, BUS & TRUCK', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131222', 0, 'AK.BANGUNAN', 1, '0000-00-00 00:00:00', '', 13, NULL, NULL),
('131301', 0, 'MOBILE APPS DANAIN', 1, '2021-01-28 10:08:01', '0', 14, NULL, NULL),
('132000', 2, 'ASET KEUANGAN TIDAK LANCAR', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('132100', 1, 'ASET TANGGUHAN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('132101', 0, 'ASET PAJAK TANGGUHAN', 1, '0000-00-00 00:00:00', '', 15, NULL, NULL),
('200000', 4, 'LIABILITAS', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('210000', 3, 'LIABILITAS JANGKA PENDEK', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('211000', 2, 'UTANG USAHA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('211101', 0, 'TITIPAN INVESTOR', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('211102', 0, 'UTANG INVESTASI PT MAS', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('211110', 0, 'UTANG WIDHRAW', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('211111', 0, 'UTANG KSP', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('211112', 0, 'UTANG SMA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('211113', 0, 'UTANG PIHAK BERELASI', 1, '2019-02-06 10:11:26', NULL, 0, NULL, NULL),
('211120', 0, 'UTANG LAIN-LAIN', 1, '2019-04-09 17:41:26', NULL, 0, NULL, NULL),
('21113', 0, 'Hutang Holding', 0, '2019-02-01 17:21:22', NULL, 0, NULL, NULL),
('212000', 2, 'UTANG PAJAK', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('212101', 0, 'Utang Pajak PPh 21', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('212102', 0, 'Utang Pajak PPh 23', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('212103', 0, 'Utang Pajak PPh 25', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('212104', 0, 'Utang Pajak PPh 29', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('212105', 0, 'Utang Pajak PPh Psl. 4 Ayat 2', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('213000', 2, 'BIAYA YANG MASIH HARUS DIBAYAR', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('213100', 1, 'JAMSOSTEK', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('213101', 0, '   JAMSOSTEK (TANGGUNGAN KARYAWAN)', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('213102', 0, '   JAMSOSTEK (TANGGUNGAN PERUSAHAAN)', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('213200', 1, 'THR DAN BONUS', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('213201', 0, 'CADANGAN THR', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('213202', 0, 'CADANGAN BONUS', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('219000', 2, 'UTANG LANCAR', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('219108', 0, 'KL POTONGAN LAINNYA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('219109', 0, 'KL POTONGAN PINJAMAN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('219199', 0, 'KL LAINNYA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('230000', 3, 'LIABILITAS JANGKA PANJANG', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('231000', 2, 'PINJAMAN BANK', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('232000', 2, 'LIABILITAS IMBALAN KERJA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('232101', 0, 'LIABILITAS IMBALAN KERJA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('300000', 4, 'EKUITAS', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('310000', 3, 'EKUITAS', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('310001', 0, 'Modal Saham', 1, '0000-00-00 00:00:00', '', 21, NULL, NULL),
('310002', 0, 'Dividen', 1, '0000-00-00 00:00:00', '', 21, NULL, NULL),
('310003', 0, 'Tambahan Modal Disetor', 1, '0000-00-00 00:00:00', '', 21, NULL, NULL),
('310004', 0, 'Agio Saham', 1, '0000-00-00 00:00:00', '', 21, NULL, NULL),
('310005', 0, 'Disagio Saham', 1, '0000-00-00 00:00:00', '', 21, NULL, NULL),
('310006', 0, 'OCI Actuarial (Gain) Loss', 1, '0000-00-00 00:00:00', '', 21, NULL, NULL),
('320000', 3, 'SALDO LABA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('320001', 0, 'LABA (RUGI) TAHUN LALU', 1, '0000-00-00 00:00:00', '', 21, NULL, NULL),
('320002', 0, 'LABA (RUGI) TAHUN BERJALAN', 1, '0000-00-00 00:00:00', '', 21, NULL, NULL),
('400000', 4, 'PENDAPATAN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('410000', 3, 'PENDAPATAN USAHA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('411000', 2, 'PENDAPATAN PLATFORM', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('411001', 0, 'PENDAPATAN JASA PLATFORM', 1, '0000-00-00 00:00:00', '', 1, NULL, NULL),
('412000', 2, 'PENDAPATAN ADMINISTRASI DAN PROVISI', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('412001', 0, 'PENDAPATAN ADMIN JASA PLATFORM - EMAS', 1, '0000-00-00 00:00:00', '', 1, NULL, NULL),
('412002', 0, 'PENDAPATAN ADMIN JASA PLATFORM - KASBON', 1, '0000-00-00 00:00:00', '', 1, NULL, NULL),
('419000', 2, 'PENDAPATAN OPERASIONAL LAINNYA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('419001', 0, 'PENDAPATAN BUNGA PINJAMAN KARYAWAN', 1, '0000-00-00 00:00:00', '', 1, NULL, NULL),
('450000', 3, 'PENDAPATAN NON OPERASIONAL', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('451000', 2, 'PENDAPATAN NON OPERASIONAL', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('451001', 0, 'PENDAPATAN JASA GIRO', 1, '0000-00-00 00:00:00', '', 1, NULL, NULL),
('451002', 0, 'PENDAPATAN JASA DEPOSITO', 1, '0000-00-00 00:00:00', '', 1, NULL, NULL),
('451999', 0, 'PENDAPATAN NON OPERASIONAL LAINNYA', 1, '0000-00-00 00:00:00', '', 1, NULL, NULL),
('500000', 4, 'BIAYA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('510000', 3, 'BIAYA BUNGA DAN KEUANGAN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('511000', 2, 'BIAYA BUNGA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('511001', 0, 'BUNGA INVESTASI', 1, '0000-00-00 00:00:00', '', 2, NULL, NULL),
('512000', 2, 'BIAYA KEUANGAN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('512001', 0, 'BIAYA ADMINISTRASI BANK', 1, '0000-00-00 00:00:00', '', 2, NULL, NULL),
('512002', 0, 'BIAYA PAJAK JASA GIRO', 1, '0000-00-00 00:00:00', '', 2, NULL, NULL),
('580000', 3, 'BIAYA NON OPERASIONAL', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('590000', 3, 'PAJAK', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('600000', 4, 'Biaya Operasional', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('620000', 3, 'BIAYA OPERASIONAL', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('621000', 2, 'BIAYA TENAGA KERJA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('621001', 0, 'Biaya Gaji dan Upah', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621002', 0, 'Tunjangan Jabatan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621003', 0, 'Tunjangan Transport', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621004', 0, 'Tunjangan Uang Makan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621005', 0, 'Tunjangan Sewa Rumah', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621006', 0, 'Tunjangan Komunikasi / HP', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621007', 0, 'Tunjangan Kemahalan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621008', 0, 'Tunjangan Kendaraan / COP', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621009', 0, 'Tunjangan BBM', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621010', 0, 'Tunjangan Pernikahan', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('621011', 0, 'Tunjangan Melahirkan', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('621012', 0, 'Tunjangan Lainnya', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('621013', 0, 'Tunjangan Pajak PPh 21', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('621014', 0, 'Biaya Honor pegawai part time / harian', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('621015', 0, 'Biaya Pengobatan Karyawan', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('621016', 0, 'Biaya Rawat Inap Karyawan', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('621017', 0, 'Biaya Tunjangan Hari Raya (THR)', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('621018', 0, 'Biaya Bonus Tahunan', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('621019', 0, 'Biaya Penghargaan', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('621020', 0, 'Biaya Insentive Penjualan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621021', 0, 'Biaya Lembur', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621022', 0, 'Biaya Premi Asuransi Kesehatan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621023', 0, 'Jamsostek Tanggung Perusahaan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621024', 0, 'Jamsostek Tanggung Karyawan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621025', 0, 'Jaminan Pensiun Tanggung Perusahaan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621026', 0, 'Jaminan Pensiun Tanggung Karyawan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621027', 0, 'Biaya BPJS Kesehatan Tanggung Perusahaan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621028', 0, 'Biaya BPJS Kesehatan Tanggung Karyawan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621029', 0, 'Biaya atas cadangan Tunjangan Hari Raya', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621030', 0, 'Biaya atas cadangan Bonus Tahunan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621031', 0, 'Biaya atas cadangan Cuti Tahunan', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621032', 0, 'Biaya atas cadangan pesangon / imbal kerja', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('621033', 0, 'Biaya Imbalan Pasca Kerja', 0, '2020-04-08 16:00:36', NULL, 0, NULL, NULL),
('621999', 0, 'Biaya Gaji Lainnya', 1, '0000-00-00 00:00:00', '', 4, NULL, NULL),
('622000', 2, 'BIAYA OPERASIONAL LAINNYA', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('622001', 0, '    LISTRIK', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622002', 0, '    TELEPON', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622003', 0, '    PULSA', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622004', 0, '    INTERNET', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622005', 0, '    AIR', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622006', 0, '    ATK', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622007', 0, '    CETAKAN', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622008', 0, '    BENSIN MOBIL OPERASIONAL', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622009', 0, '    BENSIN MOTOR', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622010', 0, '    PERJALANAN DINAS DALAM NEGERI', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622011', 0, '    PERJALANAN DINAS LUAR NEGERI', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622012', 0, '    DIGITAL MARKETING', 1, '0000-00-00 00:00:00', '', 3, NULL, NULL),
('622013', 0, '    ENTERTAINMENT', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622014', 0, '    MAKAN SIANG / MALAM', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622015', 0, ' MARKETING GIMMICK', 1, '0000-00-00 00:00:00', '', 3, NULL, NULL),
('622016', 0, '    MARKETING ACTIVITY', 1, '0000-00-00 00:00:00', '', 3, NULL, NULL),
('622017', 0, '    PENGIRIMAN/ONGKIR', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622018', 0, '    AKUISISI USER', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622019', 0, '    INVENTARIS YG TDK DIKAPITALISASI', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622020', 0, '    JAMSOSTEK TG PERUSAHAAN', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622021', 0, '    KEAMANAN', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622022', 0, '    KEPERLUAN KANTOR', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622023', 0, '    MATERAI', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622024', 0, '    PARKIR', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622025', 0, '    TRANSPORTASI', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622026', 0, '    SURAT-SURAT KENDARAAN OPERASIONAL', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622027', 0, '    RETRIBUSI & PAJAK LAINNYA', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622028', 0, '    PREMI ASURANSI AKTIVA TETAP', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622029', 0, '    PREMI ASURANSI', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622030', 0, 'KONSULTAN', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622031', 0, 'NOTARIS/PENGACARA', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622032', 0, 'BIAYA LEGAL', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622033', 0, 'KONSULTAN MANAJEMEN', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622034', 0, 'AKUNTAN', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622035', 0, 'BIAYA JASA OUTSOURCING', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622036', 0, 'BIAYA PAJAK', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622037', 0, 'BIAYA SERAGAM', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622038', 0, 'BIAYA IURAN ORGANISASI/ASOSIASI', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622039', 0, 'BIAYA KESEHATAN', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622040', 0, 'BIAYA IURAN BPJS KETENAGAKERJAAN', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('622041', 0, 'BIAYA IURAN BPJS KESEHATAN', 1, '2020-05-12 14:40:34', NULL, 5, NULL, NULL),
('622999', 0, 'Biaya Operasional Lainnya', 1, '0000-00-00 00:00:00', '', 5, NULL, NULL),
('623000', 2, 'BIAYA PEMELIHARAAN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('623001', 0, 'SERVIS & PERBAIKAN MOBIL OPERASIONAL', 1, '0000-00-00 00:00:00', '', 6, NULL, NULL),
('623002', 0, 'SERVIS & PERBAIKAN MOTOR', 1, '0000-00-00 00:00:00', '', 6, NULL, NULL),
('623003', 0, 'PERBAIKAN/PERAWATAN GEDUNG YANG DISEWA', 1, '0000-00-00 00:00:00', '', 6, NULL, NULL),
('623004', 0, 'PERBAIKAN INVENTARIS KANTOR', 1, '0000-00-00 00:00:00', '', 6, NULL, NULL),
('623005', 0, 'PEMELIHARAAN SOFTWARE & HARDWARE', 1, '0000-00-00 00:00:00', '', 6, NULL, NULL),
('624000', 2, 'BIAYA SEWA', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('624001', 0, 'SEWA GEDUNG', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('624002', 0, 'SEWA MESIN PHOTOCOPY', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('624003', 0, 'SEWA DISPENSER', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('624004', 0, 'SEWA TANAMAN', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('624005', 0, 'SEWA KENDARAAN', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('624006', 0, 'SEWA SISTEM', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('625000', 3, 'BIAYA PENYUSUTAN AKTIVA TETAP', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('625100', 2, 'BIAYA PENYUSUTAN AKTIVA TETAP GOL1', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('625101', 0, '1A-MEBEL & PERALATAN KAYU', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625102', 0, '1B-MESIN KANTOR(KOMP,PRINTER,S', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625103', 0, '1C-KAMERA & VIDEO', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625104', 0, '1D-SEPEDA MOTOR', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625105', 0, '1E-ALAT PERLENGKAPAN KHUSUS', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625106', 0, '1F-ALAT DAPUR', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625200', 2, 'BIAYA PENYUSUTAN AKTIVA TETAP GOL2', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('625201', 0, '2A-MEBEL & PERLENGKAPAN LOGAM', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625202', 0, '2B-AC & KIPAS ANGIN', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625204', 0, '2D-MOBIL, BUS & TRUK', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625205', 0, '2E-CONTAINER', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625300', 2, 'BIAYA BANGUNAN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('625301', 0, 'BANGUNAN', 1, '0000-00-00 00:00:00', '', 7, NULL, NULL),
('625401', 0, 'BIAYA PENYUSUTAN MOBILE APPS danaIN', 1, '2021-01-26 12:47:02', '0', 8, NULL, NULL),
('627000', 2, 'BIAYA NON OPERASIONAL', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('627001', 0, '    TRAINING', 1, '0000-00-00 00:00:00', '', 9, NULL, NULL),
('627002', 0, '    SEMINAR', 1, '0000-00-00 00:00:00', '', 9, NULL, NULL),
('627003', 0, '    RAKER', 1, '0000-00-00 00:00:00', '', 9, NULL, NULL),
('700000', 4, 'PENDAPATAN & BIAYA NON OPERASIONAL', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('710000', 3, 'PENDAPATAN NON OPERASIONAL', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('711000', 2, 'PNO - SELISIH PEMBULATAN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('711199', 0, 'PNO - SELISIH PEMBULATAN', 1, '0000-00-00 00:00:00', '', 9, NULL, NULL),
('720000', 3, 'BIAYA NON OPERASIONAL', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('721000', 2, 'BNO - SELISIH PEMBULATAN', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('721199', 4, 'BNO - SELISIH PEMBULATAN', 0, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('800000', 2, 'PAJAK', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('800001', 0, '    PAJAK PERSEROAN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('800002', 0, '    PAJAK KINI', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('800003', 0, '    MANFAAT PAJAK TANGGUHAN', 1, '0000-00-00 00:00:00', '', 0, NULL, NULL),
('900000', 4, 'REKENING ADMINISTRATIF', 1, '2019-06-17 12:33:17', NULL, 0, NULL, NULL),
('910000', 3, 'ADMINISTRATIF - SALDO DEBET', 1, '2019-06-17 12:33:55', NULL, 0, NULL, NULL),
('911000', 2, 'ADMINISTRATIF BANK', 1, '2019-06-17 12:38:04', NULL, 0, NULL, NULL),
('911001', 0, 'ADM- BANK BCA VIRTUAL ACCOUNT', 1, '2019-06-17 12:38:16', NULL, 0, NULL, NULL),
('911002', 0, 'ADM- BANK PERMATA VIRTUAL ACCOUNT', 1, '2019-06-17 12:38:28', NULL, 0, NULL, NULL),
('911003', 0, 'ADM- BANK BCA ESCROW', 1, '2019-06-17 12:38:38', NULL, 0, NULL, NULL),
('914000', 2, 'ADMINISTRATIF PIUTANG', 1, '2019-06-17 12:38:52', NULL, 0, NULL, NULL),
('914001', 0, 'ADM - PIUTANG MITRA MAS', 1, '2019-06-17 12:39:04', NULL, 0, NULL, NULL),
('914011', 0, 'ADM KONTRA ACCOUNT ASET', 1, '2019-06-17 12:50:33', NULL, 0, NULL, NULL),
('918000', 2, 'ADMINISTRATIF KONTRA ACCOUNT BIAYA DAN PENDAP', 1, '2019-06-17 12:50:46', NULL, 0, NULL, NULL),
('918001', 1, 'ADM - BIAYA ADMIN DAN JASA GIRO VA BCA', 1, '2019-06-17 12:50:56', NULL, 0, NULL, NULL),
('918002', 0, 'ADM - BIAYA ADMIN DAN JASA GIRO VA PERMATA', 1, '2019-06-17 12:51:09', NULL, 0, NULL, NULL),
('920000', 3, 'ADMINISTRATIF - SALDO KREDIT', 1, '2019-06-17 12:51:20', NULL, 0, NULL, NULL),
('921000', 2, 'ADMINISTRATIF HUTANG', 1, '2019-06-17 12:52:00', NULL, 0, NULL, NULL),
('921001', 0, 'ADM - SALDO PENDANA', 1, '2019-06-17 12:52:51', NULL, 0, NULL, NULL),
('921002', 0, 'ADM - HUTANG MITRA MAS', 1, '2019-06-17 12:53:05', NULL, 0, NULL, NULL),
('921003', 0, 'ADM - HUTANG MITRA SMA', 1, '2019-06-17 12:54:46', NULL, 0, NULL, NULL),
('921051', 0, 'ADM - HUTANG ASURANSI', 1, '2019-06-17 13:05:32', NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblmenu`
--

CREATE TABLE `tblmenu` (
  `idMenu` int(11) NOT NULL,
  `headMenu` int(11) NOT NULL,
  `subMenu` int(11) NOT NULL,
  `childMenu` int(11) NOT NULL,
  `namaMenu` varchar(45) NOT NULL,
  `link` varchar(35) NOT NULL,
  `nourut` int(11) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblmenu`
--

INSERT INTO `tblmenu` (`idMenu`, `headMenu`, `subMenu`, `childMenu`, `namaMenu`, `link`, `nourut`, `isActive`) VALUES
(1, 1, 0, 0, 'Accounting', '#', 0, 1),
(2, 1, 2, 0, 'Parameter', '#', 1, 1),
(3, 3, 2, 0, 'Jurnal', '#', 2, 1),
(4, 1, 2, 1, 'Chart Of Account', 'Acc_mcoa', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblmodul`
--

CREATE TABLE `tblmodul` (
  `idModul` int(11) NOT NULL,
  `namaModul` varchar(25) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblmodul`
--

INSERT INTO `tblmodul` (`idModul`, `namaModul`, `isActive`) VALUES
(1, 'Administrator', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblmodul_list`
--

CREATE TABLE `tblmodul_list` (
  `idModul` int(11) NOT NULL,
  `idMenu` int(11) NOT NULL,
  `headMenu` int(11) NOT NULL,
  `subMenu` int(11) NOT NULL,
  `childMenu` int(11) NOT NULL,
  `nourut` int(11) NOT NULL,
  `tgl_proses` date NOT NULL,
  `userId` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblmodul_list`
--

INSERT INTO `tblmodul_list` (`idModul`, `idMenu`, `headMenu`, `subMenu`, `childMenu`, `nourut`, `tgl_proses`, `userId`) VALUES
(1, 1, 1, 0, 0, 0, '2020-11-14', 'admin@danain.co.id'),
(1, 2, 1, 2, 0, 1, '2020-11-14', 'admin@danain.co.id'),
(1, 4, 1, 2, 1, 1, '2020-11-14', 'admin@danain.co.id');

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `idUser` int(11) NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '0',
  `userName` varchar(100) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL DEFAULT '0',
  `idModul` int(11) DEFAULT NULL,
  `isActive` tinyint(4) DEFAULT NULL,
  `userEdit` varchar(25) DEFAULT NULL,
  `hp` varchar(25) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`idUser`, `email`, `userName`, `password`, `idModul`, `isActive`, `userEdit`, `hp`, `created_at`, `updated_at`) VALUES
(1, 'admin@danain.co.id', 'administrator', '$2y$10$fKKkziaJio8SxGoTfc.mmeEgdXPvMXPC2kmtgil7y6unaIC1iVPMW', 1, 1, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dt_tran`
--
ALTER TABLE `dt_tran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neraca_group`
--
ALTER TABLE `neraca_group`
  ADD PRIMARY KEY (`idGroup`);

--
-- Indexes for table `parent_group`
--
ALTER TABLE `parent_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profit_group`
--
ALTER TABLE `profit_group`
  ADD PRIMARY KEY (`idGroup`);

--
-- Indexes for table `siskon`
--
ALTER TABLE `siskon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sm_tran`
--
ALTER TABLE `sm_tran`
  ADD PRIMARY KEY (`noBatch`,`prdbln`,`prdthn`),
  ADD KEY `noBatch` (`noBatch`);

--
-- Indexes for table `tblcoa`
--
ALTER TABLE `tblcoa`
  ADD PRIMARY KEY (`kodeCoa`);

--
-- Indexes for table `tblmenu`
--
ALTER TABLE `tblmenu`
  ADD PRIMARY KEY (`idMenu`);

--
-- Indexes for table `tblmodul`
--
ALTER TABLE `tblmodul`
  ADD PRIMARY KEY (`idModul`);

--
-- Indexes for table `tblmodul_list`
--
ALTER TABLE `tblmodul_list`
  ADD KEY `idModul` (`idModul`,`headMenu`),
  ADD KEY `idMenu` (`idMenu`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dt_tran`
--
ALTER TABLE `dt_tran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `neraca_group`
--
ALTER TABLE `neraca_group`
  MODIFY `idGroup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `parent_group`
--
ALTER TABLE `parent_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `profit_group`
--
ALTER TABLE `profit_group`
  MODIFY `idGroup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `siskon`
--
ALTER TABLE `siskon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblmenu`
--
ALTER TABLE `tblmenu`
  MODIFY `idMenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblmodul`
--
ALTER TABLE `tblmodul`
  MODIFY `idModul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
