const { default: Axios } = require("axios")

    $(document).ready(function() {
        $('#add-journal-button').on('click', function() {
            let today = new Date();
            let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            let dateTime = date+' '+time;
            $('#journal-date').val(dateTime);
            $('#add-journal-modal').modal('show');
            
        })
        $('.number-format').mask("#.##0", {reverse: true});
    
        $('#add-journal-form').on('submit', function(event) {
            $(event.preventDefault());
            $('.number-format').unmask();
            $('#add-journal-submit').text('Loading ...').attr('disabled', true);
            Axios({
                url: '/manual-journals',
                method: 'post',
                data: $('#add-journal-form').serialize()
            }).then(function(response) {
                // console.log(response)
                Swal.fire({
                    icon: response.data.message.icon,
                    title: response.data.message.title,
                    text: response.data.message.text,
                }).then(function() {
                    location.reload()
                })
            }).catch(function(error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Terdapat kesalahan',
                    text: 'Silahkan input manual jurnal kembali',
                })
            }).finally(function() {
                $('#add-journal-submit').text('Simpan').attr('disabled', false);
            })
        })

        $('#add-journal-modal').on('hidden.bs.modal', function() {
            $('#add-journal-form')[0].reset();
        })

        // Journal Table
        $('#journal-table').DataTable({
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url: '/manual-journal-tables',
                method: 'get',
            },
            columns: [
                { data: 'DT_RowIndex', searchable: false, orderable: false },
                { data: 'period'}, // raw column
                { data: 'tgl_proses'}, // get from DB
                { data: 'jurName'}, // get from DB
                { data: 'keterangan'}, // get from DB
                { data: 'amount'}, // raw column
                { data: 'post'}, // raw column
                { data: 'delete'}, // raw column
            ],
            order: [1, 'asc'],
        })
        
        $(document).on('click', '.delete-journals', function(event) {

            let journalId = $(event.target).data('id');

            if (journalId  == null) {
                journalId = $(event.target).parents('.delete-journals').data('id');
            }

            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Data Anda Tidak Akan Kembali!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus Jurnal!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Axios ({
                        method: 'delete',
                        url: `/manual-journals/${journalId}`
                    }).then(function(response) {
                        Swal.fire({
                            icon: response.data.message.icon,
                            title: response.data.message.title,
                            text: response.data.message.text,
                        }).then(function() {
                            $('#journal-table').DataTable().ajax.reload();
                        })
                    }).catch(function(error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Terdapat kesalahan',
                            text: 'Silahkan input manual jurnal kembali',
                        }).then(function() {
                            location.reload();
                        })
                    }).finally(function(){});
                }
            })
        })

        $(document).on('click', '.post-manual-journals' , function(event) {
            let postJournalId = $(event.target).data('id')

            if (postJournalId  == null) {
                postJournalId = $(event.target).parents('.post-manual-journals').data('id')
            }

            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Data Akan Pindah Ke Jurnal Posting",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Posting Jurnal'
            }).then((result) => {
                if (result.isConfirmed) {
                    Axios ({
                        method: 'put',
                        url: `/manual-journals/${postJournalId}`
                    }).then(function(response) {
                        Swal.fire({
                            icon: response.data.message.icon,
                            title: response.data.message.title,
                            text: response.data.message.text,
                        }).then(function() {
                            $('#journal-table').DataTable().ajax.reload()
                        })
                    }).catch(function(error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Terdapat kesalahan',
                            text: 'Silahkan input manual jurnal kembali',
                        }).then(function() {
                            location.reload()
                        })
                    }).finally(function(){})
                }
            })
        })
    })
