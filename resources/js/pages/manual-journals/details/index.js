const { default: Axios } = require("axios")

$(document).ready(function() {
    $('#coa-select').select2({
        placeholder: 'Pilih Kode COA',
    });
    
    $('#journal-detail-table').DataTable();
    
    $('.number-format').mask("#.##0", {reverse: true});

    $('#add-new-journal').on('submit', function(event) {
        $(event.preventDefault())

        $('#submit-journal-btn').text('Loading ...').attr('disabled', true)

        let journalDetailId = $(event.target).data('id')

        $('.number-format').unmask();

        Axios({
            method: 'post',
            url: `/manual-journals/${journalDetailId}/details`,
            data: $('#add-new-journal').serialize()
        }).then(function(response) {
            Swal.fire({
                icon: response.data.message.icon,
                title: response.data.message.title,
                text: response.data.message.text,
            }).then(function() {
                location.reload()
            })
        }).catch(function(error) {
            Swal.fire({
                icon: 'error',
                title: 'Terdapat kesalahan',
                text: 'Silahkan input manual jurnal kembali',
            })
        }).finally(function() {
            $('#submit-journal-btn').text('Submit').attr('disabled', false)
        })
    })

    $('.delete-journal-details').on('click', function(event) {
        let deleteJournalDetailId = $(event.target).data('id')

        if (deleteJournalDetailId  == null) {
            deleteJournalDetailId = $(event.target).parents('.delete-journal-details').data('id')
        }
        
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data Anda Tidak Akan Kembali!",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus Jurnal!'
          }).then((result) => {
            if (result.isConfirmed) {
                Axios ({
                    method: 'delete',
                    url: `/manual-journal-details/${deleteJournalDetailId}`
                }).then(function(response) {
                    Swal.fire({
                        icon: response.data.message.icon,
                        title: response.data.message.title,
                        text: response.data.message.text,
                    }).then(function() {
                        location.reload()
                    })
                }).catch(function(error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Terdapat kesalahan',
                        text: 'Silahkan input manual jurnal kembali',
                    }).then(function() {
                        location.reload()
                    })
                }).finally(function(){})
            }
        })
    })
})