const { default: Axios } = require("axios")

$(document).ready(function() {

    $('#login').submit(function(e) {
        $(e.preventDefault())
        $('#submit-login').attr('disabled', true).text('Loading ...')

        Axios({
            method: 'post',
            url: "/login",
            data: $('#login').serialize()
        }).then(function(response) {
            $('.notification').html(`
            <div class="alert alert-success">
                ${response.data}
            </div>
            `)
            location.reload();
        }).catch(function(error) {
            $('.notification').html(`
            <div class="alert alert-danger">
                ${error.response.data}
            </div>
            `)
        }).finally(function() {
            $('#submit-login').attr('disabled', false).text('Sign In')
        })
    })

})