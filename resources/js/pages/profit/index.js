$(document).ready(function() {
    
    $('#filter-profit-form').submit(function(e) {
        $(e.preventDefault())
        
        $('#profit-table').DataTable().ajax.reload()
    })
    $('#profit-table').DataTable({
        searching: false,
        paging: false,
        info: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            url: '/profit-tables',
            method: 'get',
            data: function(data) {
                data.group = $('select[name=filter-group]').val()
                data.month = $('select[name=filter-month]').val()
                data.year = $('select[name=filter-year]').val()
            }
        },
        columns: [
            { data: 'DT_RowIndex', searchable: false, orderable: false },
            { data: 'kodeCoa', name: 'kodeCoa'},
            { data: 'keterangan', name: 'keterangan'},
            { data: 'mutasi', name: 'mutasi', render: $.fn.dataTable.render.number('.','.')},
        ],

        'footerCallback': function() {
            let api = this.api()

            total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    let x = parseFloat(a) || 0
                    let y = parseFloat(b) || 0 
                    return x + y 
                }, 0)
            if (total < 0) {
                $( api.column( 3 ).footer() ).html(total).mask('-'+'000.000.000.000.000,00', total)
            } else {
                $( api.column( 3 ).footer() ).html(total).mask('000.000.000.000.000', total)
            }
        }

    })
})