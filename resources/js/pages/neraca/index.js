$(document).ready(function() {
    
    $('#filter-neraca-form').submit(function(e) {
        $(e.preventDefault())
        
        $('#neraca-table').DataTable().ajax.reload()
    })
    $('#neraca-table').DataTable({
        searching: false,
        paging: false,
        info: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            url: '/neraca-tables',
            method: 'get',
            data: function(data) {
                data.group = $('select[name=filter-group]').val()
                data.month = $('select[name=filter-month]').val()
                data.year = $('select[name=filter-year]').val()
            }
        },
        columns: [
            { data: 'DT_RowIndex', searchable: false, orderable: false },
            { data: 'kodeCoa', name: 'kodeCoa'},
            { data: 'keterangan', name: 'keterangan'},
            { data: 'saldoAkhir', name: 'saldoAkhir', className: 'sum', render: $.fn.dataTable.render.number('.','.')},
            { data: 'mutasi', name: 'mutasi', className: 'sum', render: $.fn.dataTable.render.number('.','.')},
            
        ],

        'footerCallback': function() {
            let api = this.api()

            api.columns('.sum').every(function () {
                let sum = this
                    .data()
                    .reduce(function (a, b) {
                        let x = parseFloat(a) || 0
                        let y = parseFloat(b) || 0 
                        return x + y 
                    }, 0)
                // alert(sum)
                if (sum < 0 ) {
                    $(this.footer()).html(sum).mask('-'+'000.000.000.000.000', sum)
                }
            })
        }
        
    })
    
})