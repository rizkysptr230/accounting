$(document).ready(function() {

    $('#filter-trial-balance-form').submit(function(e) {
        $(e.preventDefault())

        $('#trial-balance-table').DataTable().ajax.reload()
    })
    $('#trial-balance-table').DataTable({
        searching: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            url: '/trial-balance-tables',
            method: 'get',
            data: function(data) {
                data.month = $('select[name=filter-month]').val()
                data.year = $('select[name=filter-year]').val()
            }
        },
        columns: [
            { data: 'DT_RowIndex', searchable: false, orderable: false },
            { data: 'kodeCoa', name: 'kodeCoa'},
            { data: 'keterangan', name: 'keterangan'},
            { data: 'saldo', name: 'saldo', render: $.fn.dataTable.render.number('.','.')},
            { data: 'debit', name: 'debit', render: $.fn.dataTable.render.number('.','.')},
            { data: 'kredit', name: 'kredit', render: $.fn.dataTable.render.number('.','.')},
            { data: 'saldoAkhir', name: 'saldoAkhir', render: $.fn.dataTable.render.number('.','.')},
            { data: 'mutasi', name: 'mutasi', render: $.fn.dataTable.render.number('.','.')},
        ],
        
    })
})