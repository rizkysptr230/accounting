$(document).ready(function() {

    $('#posting-journal-table').DataTable()
    $(document).on('click', '.delete-journals', function(event) {

        let journalId = $(event.target).data('id')

        if (journalId  == null) {
            journalId = $(event.target).parents('.delete-journals').data('id')
        }

        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data Anda Tidak Akan Kembali!",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus Jurnal!'
          }).then((result) => {
            if (result.isConfirmed) {
                Axios ({
                    method: 'delete',
                    url: `/manual-journals/${journalId}`
                }).then(function(response) {
                    Swal.fire({
                        icon: response.data.message.icon,
                        title: response.data.message.title,
                        text: response.data.message.text,
                    }).then(function() {
                        $('#journal-table').DataTable().ajax.reload()
                    })
                }).catch(function(error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Terdapat kesalahan',
                        text: 'Silahkan input manual jurnal kembali',
                    }).then(function() {
                        location.reload()
                    })
                }).finally(function(){})
            }
        })
    })
})