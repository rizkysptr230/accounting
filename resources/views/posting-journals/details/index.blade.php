@extends('layouts.app')

@section('content')
{{-- content-header --}}
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <a href="{{ url('/posting-journals') }}" style="font-size: 1.5em;"><i class="fas fa-arrow-left"></i></a>
            <div class="col-sm-6">
                <h1>Jurnal Detail</h1>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <div class="row">
                            <h6 class="card-title"><i class="fas fa-list"></i> Nama Jurnal :{{$journalTitle['jurName']}}, Keterangan : {{$journalTitle['keterangan']}}, Nominal : {{$journalTitle['amount']}}</h6>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="journal-detail-table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal</th>
                                    <th>Kode COA</th>
                                    <th>D/K</th>
                                    <th>Nominal</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php($j=1)
                                @foreach($dtTranData as $dtTran)
                                <tr>
                                    <td>{{$j++}}</td>
                                    <td>{{$dtTran->tgl_validasi}}</td>
                                    <td>{{$dtTran->kodeCoa}}</td>
                                    <td>{{$dtTran->dk}}</td>
                                    <td>{{$dtTran->amount}}</td>
                                    <td>{{$dtTran->keterangan}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">Total</td>
                                    <td colspan="2">{{number_format($totalPrice, 2)}}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->



@endsection


@section('css')
<link rel="stylesheet" href="{{url('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('js')
<script src="{{url('assets/plugins/datatables/jquery.dataTables.js') }}" defer></script>
<script src="{{url('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}" defer></script>
<script src="{{asset('/js/pages/posting-journals/details/index.js')}}" type="text/javascript" defer></script>
@endsection
