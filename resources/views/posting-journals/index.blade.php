@extends('layouts.app')

@section('content')
{{-- content-header --}}
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Jurnal Posting</h1>
            </div>
        </div>
    </div>
</div>


<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <div class="row">
                            <h5 class="card-title m-0">Semua Data</h5>
                        </div>
                    </div>
                    <div class="card-body table-responsive">
                        <table id="posting-journal-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Periode</th>
                                    <th>Tanggal</th>
                                    <th>Nama Jurnal</th>
                                    <th>Keterangan</th>
                                    <th>Nominal</th>
                                    {{-- <th>Posting</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posting as $post)
                                    @php
                                        $batch = $post['batch'];
                                    @endphp
                                    <tr>
                                        <td>{{$post['no']}}</td>
                                        <td>{{$post['period']}}</td>
                                        <td>{{$post['date']}}</td>
                                        <td>{{$post['name']}}</td>
                                        <td>{{$post['description']}}</td>
                                        <td><a href="{{ url("/posting-journals/$batch/details") }}">{{$post['amount']}}</a></td>
                                        {{-- <td>
                                        <center>
                                            <div class="btn btn-danger delete-journals" data-id="{{$batch}}">
                                                <i class="fas fa-trash"></i>
                                            </div>
                                        </center>
                                        </td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css') }}">
@endsection

@section('js')
<script src="{{url('https://code.jquery.com/jquery-3.5.1.js') }}"></script>
<script src="{{url('https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js') }}" defer></script>
<script src="{{asset('/js/pages/posting-journals/index.js')}}" type="text/javascript" defer></script>
@endsection