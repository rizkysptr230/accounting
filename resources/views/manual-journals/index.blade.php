@extends('layouts.app')

@section('content')
{{-- content-header --}}
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Jurnal Manual</h1>
            </div>
        </div>
    </div>
</div>


<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <div class="row">
                            <h1 class="card-title m-0">Semua Data</h1>
                            {{-- <a class="col-sm-3 text-right">
                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#add-journal-modal">Tambah Data</button>
                            </a> --}}
                                <a href='javascript:;' style="text-align:right; float:right; width:90%;" id="add-journal-button"><i class="fas fa-plus-circle mr-2"></i>Tambah Data</a>
                        </div>
                    </div>
                    <div class="card-body table-responsive">
                        <table id="journal-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Periode</th>
                                    <th>Tanggal</th>
                                    <th>Nama Jurnal</th>
                                    <th>Keterangan</th>
                                    <th>Nominal</th>
                                    <th>Posting</th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

{{-- <div class="modal fade" id="add-journal-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Tambah Data</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form id="add-journal-form">
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Tanggal</label>
                    <input class="form-control" type="text" name="date" id="journal-date" placeholder="Pilih Tanggal" readonly required>
                </div>
                <div class="form-group col-md-12">
                    <label>Nama Jurnal</label>
                    <input class="form-control" type="text" name="name" id="journal-name" placeholder="Nama Jurnal" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Keterangan</label>
                    <textarea class="form-control" name="description" id="journal-description" rows="3" placeholder="Keterangan Jurnal" required></textarea>
                </div>
                <div class="form-group col-md-12">
                    <label>Nominal</label>
                    <input class="form-control" type="number" min="0" name="value" id="formatNumber" placeholder="Nominal" required>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success" id="add-journal-submit">Simpan</button>
        </div>
      </div>
    </form>
    </div>
  </div> --}}

{{-- add journal modal --}}
<div class="modal fade" tabindex="-1" id="add-journal-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tambah Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="add-journal-form">
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Tanggal</label>
                        <input class="form-control" type="text" name="date" id="journal-date" placeholder="Pilih Tanggal" readonly required>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Periode</label>
                        <input class="form-control" type="month" name="period" id="journal-period" min="2020-01" placeholder="Pilih Periode" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Nama Jurnal</label>
                        <input class="form-control" type="text" name="name" id="journal-name" placeholder="Nama Jurnal" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Keterangan</label>
                        <textarea class="form-control" name="description" id="journal-description" rows="3" placeholder="Keterangan Jurnal" required></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Nominal</label>
                        <input class="form-control number-format" type="tel" min="0" name="value" placeholder="Nominal" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-success" id="add-journal-submit">Simpan</button>
            </div>
        </form>
        </div>
    </div>
</div>


  @endsection
  
@section('css')

<link rel="stylesheet" href="{{url('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{url('https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{url('assets/plugins/sweetalert2/sweetalert2.min.css') }}">

@endsection

@section('js')

<script src="{{url('https://code.jquery.com/jquery-3.5.1.js') }}" defer></script>
<script src="{{url('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js') }}" defer></script>
<script src="{{url('https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js') }}" defer></script>
<script src="{{url('https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js') }}" defer></script>
<script src="{{url('assets/plugins/sweetalert2/sweetalert2.all.js') }}" defer></script>
<script src="{{asset('/js/pages/manual-journals/index.js')}}" type="text/javascript" defer></script>
@endsection

