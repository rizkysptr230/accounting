@extends('layouts.app')

@section('content')
{{-- content-header --}}
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <a href="{{ url('/manual-journals') }}" style="font-size: 1.5em;"><i class="fas fa-arrow-left"></i></a>
            <div class="col-sm-6">
                <h1>Jurnal Detail</h1>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <div class="row">
                            <h6 class="card-title font-weight-bold"><i class="fas fa-list"></i> Nama Jurnal :{{$journalTitle['jurName']}}, Keterangan : {{$journalTitle['keterangan']}}, Nominal : {{number_format($journalTitle['amount'],2)}}</h6>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mb-5">
                            <div class="col-md-12">
                            <form id="add-new-journal" data-id="{{$noBatch}}">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Kode COA</label>
                                                <select class="form-control" id="coa-select" name="coa-code" required>
                                                    @foreach ($coaOption as $option)
                                                        <option value="{{$option['kodeCoa']}}"> {{$option['kodeCoa']}} - {{$option['keterangan']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Keterangan</label>
                                                <input type="text" class="form-control" name="description" placeholder="Keterangan">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Debit/Kredit</label>
                                                <select class="form-control" id="dk-select" name="debit-or-credit" required>
                                                    <option value="K">Kredit</option>
                                                    <option value="D" selected>Debit</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nominal</label>
                                                <input type="tel" class="form-control number-format" name="value" placeholder="Nominal" required>
                                                <button type="submit" class="btn btn-success mt-4 mx-2" id="submit-journal-btn">Simpan</button>
                                                <a href="{{ url('/manual-journals') }}" class="btn btn-secondary mt-4">Kembali</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div class="table-responsive">
                        <table id="journal-detail-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal</th>
                                    <th>Kode COA</th>
                                    <th>D/K</th>
                                    <th>Nominal</th>
                                    <th>Keterangan</th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php($j=1)
                                @foreach($dtTranData as $dtTran)
                                <tr>
                                    <td>{{$j++}}</td>
                                    <td>{{$dtTran->tgl_validasi}}</td>
                                    <td>{{$dtTran->kodeCoa}}</td>
                                    <td>{{$dtTran->dk}}</td>
                                    <td>{{number_format($dtTran->amount, 2)}}</td>
                                    <td>{{$dtTran->keterangan}}</td>
                                    <td>
                                        <center>
                                        <a href="javascript:;" class="delete-journal-details" data-id="{{$dtTran->id}}">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                        </center>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">Total</td>
                                    <td colspan="3">{{number_format($totalPrice, 2)}}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->



@endsection

@section('css')

<link rel="stylesheet" href="{{url('https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{url('assets/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{url('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{url('assets/plugins/sweetalert2/sweetalert2.min.css') }}">

@endsection

@section('js')

<script src="{{url('https://code.jquery.com/jquery-3.5.1.js') }}" defer></script>
<script src="{{url('https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js') }}" defer></script>
<script src="{{url('https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js') }}" defer></script>
<script src="{{url('assets/plugins/select2/js/select2.min.js')}}" type="text/javascript" defer></script>
<script src="{{url('assets/plugins/sweetalert2/sweetalert2.all.js') }}" defer></script>
<script src="{{asset('/js/pages/manual-journals/details/index.js')}}" type="text/javascript" defer></script>
@endsection
