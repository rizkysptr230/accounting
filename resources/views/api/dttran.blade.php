@extends('layouts.app')

@section('content')
{{-- content-header --}}
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <a href="{{ url('/home') }}" style="font-size: 1.5em;"><i class="fas fa-arrow-left"></i></a>
            <div class="col-sm-6">
                <h1>API DtTran</h1>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <div class="row">
                            <h5 class="card-title m-0">Semua Data</h5>
                        </div>
                    </div>
                    <div class="card-body table-responsive">
                        <table id="api-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>noBatch</th>
                                    <th>prdbln</th>
                                    <th>prdthn</th>
                                    <th>nourut</th>
                                    <th>kodecoa</th>
                                    <th>keterangan</th>
                                    <th>d/k</th>
                                    <th>amount</th>
                                    <th>tgl_proses</th>
                                    <th>tgl_validasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dtTran as $data)
                                <tr>
                                    <td>{{$data->noBatch}}</td>
                                    <td>{{$data->prdbln}}</td>
                                    <td>{{$data->prdthn}}</td>
                                    <td>{{$data->nourut}}</td>
                                    <td>{{$data->kodeCoa}}</td>
                                    <td>{{$data->keterangan}}</td>
                                    <td>{{$data->dk}}</td>
                                    <td>{{$data->amount}}</td>
                                    <td>{{$data->tgl_proses}}</td>
                                    <td>{{$data->tgl_validasi}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css') }}">
@endsection

@section('js')
<script src="{{url('https://code.jquery.com/jquery-3.5.1.js') }}"></script>
<script src="{{url('https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js') }}" defer></script>
@endsection