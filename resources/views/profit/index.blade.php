@extends('layouts.app')

@section('content')
{{-- content-header --}}
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Laba Rugi</h1>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <div class="row">
                            <h6 class="card-title font-weight-bold"></h6>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="filter-profit-form">                      
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label> Coa </label>
                                        <select class="form-control" name="filter-group">
                                            @foreach ($group as $profitGroup)
                                                <option value="{{$profitGroup['idGroup']}}">{{$profitGroup['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label> Bulan </label>
                                        <select class="form-control" name="filter-month">
                                            @foreach ($month as $keyMonth => $months)
                                                @if ($defaultData)
                                                    @if ($defaultData['month'] == $keyMonth)
                                                    <option value="{{$keyMonth}}" selected>{{$months}}</option>
                                                    @else
                                                    <option value="{{$keyMonth}}">{{$months}}</option>
                                                    @endif
                                                @else
                                                    <option value="{{$keyMonth}}">{{$months}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label> Tahun </label>
                                        <select class="form-control" name="filter-year">
                                            @foreach ($year as $yearOption)
                                                @if ($defaultData)
                                                    @if ($defaultData['year'] == $year)
                                                    <option value="{{$year}}" selected>{{$year}}</option>
                                                    @else
                                                    <option value="{{$yearOption}}">{{$yearOption}}</option>
                                                    @endif
                                                @else
                                                    <option value="{{$yearOption}}">{{$yearOption}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 mt-4 pt-2">
                                    <button class="btn btn-secondary" type="submit">Search</button>
                                </div>        
                            </div>
                        </form>  
                        <div class="table-responsive">
                            <table id="profit-table" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Coa</th>
                                        <th>Keterangan</th>
                                        <th>Saldo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>   
                                <tfoot>
                                    <tr>
                                        <td colspan="3">Total</td>
                                        <td colspan="1"></td>
                                    </tr>
                                </tfoot> 
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.content -->

@endsection

@section('css')

<link rel="stylesheet" href="{{url('https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{url('assets/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{url('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{url('assets/plugins/sweetalert2/sweetalert2.min.css') }}">

@endsection

@section('js')

<script src="{{url('https://code.jquery.com/jquery-3.5.1.js') }}" defer></script>
<script src="{{url('https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js') }}" defer></script>
<script src="{{url('https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js') }}" defer></script>
<script src="{{asset('/assets/plugins/select2/js/select2.min.js')}}" type="text/javascript" defer></script>
<script src="{{asset('/assets/plugins/sweetalert2/sweetalert2.all.js') }}" defer></script>
<script src="{{asset('/js/pages/profit/index.js')}}" type="text/javascript" defer></script>

@endsection
