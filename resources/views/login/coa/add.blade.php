@extends('layouts.app')


@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <a href="{{ route('login.coa') }}" style="font-size: 1.5em;"><i class="fas fa-arrow-left"></i></a>
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Chart Of Account</h1>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
          @if(session('debugcode')=='1')
              <div class="alert alert-success">
                {{ session('message') }}
              </div>   
            @endif
            @if(session('debugcode')=='2')
              <div class="alert alert-danger">
                {{ session('message') }}
              </div>   
            @endif
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">New Coa</h3>
              </div>
              <!-- /.card-header -->
              
              <!-- form start -->
              <form action="{{ route('login.postcoa') }}" method="post">
                @csrf
                <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Kode Coa</label>
                      <input name="kodeCoa" type="text" class="form-control" placeholder="Kode Coa"">
                    </div>
                    <div class="form-group">
                      <label>Parent</label>
                      <select name="nourut" class="custom-select">
                        <option value="0">Parent 0</option>
                        <option value="1">Parent 1</option>
                        <option value="2">Parent 2</option>
                        <option value="3">Parent 3</option>
                        <option value="4">Parent 4</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Keterangan</label>
                      <input name="keterangan" type="text" class="form-control" placeholder="Keterangan" ">
                    </div>
                    <div class="form-group">
                      <label>is Active</label>
                      <select name="isActive" class="custom-select">
                        <option value="1">Active</option>
                        <option value="0">Disabled</option>
                      </select>
                    </div>
                  </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" style="float: right;">Submit</button>
                </div>
              </form>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection