@extends('layouts.app')

@section('content')
    {{-- dddd --}}
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Chart Of Account</h1>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <div class="row">
                    <h5 class="card-title m-0">Semua Data</h5>
                    <a href='{{route('login.newcoa')}}' style="text-align:right; float:right; width:90%;"><i class="fas fa-plus-circle mr-2"></i>Tambah Data</a>
                </div>
              </div>
              <div class="card-body">
                <table id="coa-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Kode Coa</th>
                        <th>Parent</th>
                        <th>Keterangan</th>
                        <th><center>Status</center></th>
                        <th><center>Action</center></th>
                    </tr>
                </thead>
                <tbody>
                    @php($j=1)
                    @foreach($data as $d)
                    <tr>
                        <td>{{$j++}}</td>
                        <td>{{$d->kodeCoa}}</td>
                        <td>Parent {{$d->nourut}}</td>
                        <td>
                            @for ($i=1;$i<=4-$d->nourut;$i++)
                                &nbsp;
                            @endfor
                            {{$d->keterangan}}
                        </td>
                        <td><center>{{$d->isActive == 1 ? 'Active' : 'Disabled'}}</center></td>
                        <td><center><a href="{{ route('login.getcoa', ['id' => $d->kodeCoa]) }}"><span class="fa fa-fw fa-edit" ></span></a></center></td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css') }}">
@endsection
@section('js')
<!-- DataTables -->
<script src="{{url('https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js') }}" defer></script>
<script src="{{asset('/js/pages/coa/index.js')}}" type="text/javascript" defer></script>


@endsection
