<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class tblmenu extends Model
{
    protected $table = "tblmenu";
    protected $primaryKey = 'idMenu';
    protected $fillable = [
        'idMenu', 'headMenu', 'subMenu', 'childMenu', 'namaMenu', 'link', 'nourut', 'isActive'
    ];

    static function getNameByIdMenu($id){
        $data=tblmenu::where('idMenu',$id)
        ->first();
        return $data->namaMenu;
    }
}
