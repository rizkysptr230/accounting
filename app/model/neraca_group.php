<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class neraca_group extends Model
{
    protected $table = 'neraca_group';
    protected $fillable = [];

    public function neracaGroup ()
    {
        return $this->hasOne('App\model\tblcoa', 'idGroup', 'idGroup');
    }
}
