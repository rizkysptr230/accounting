<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class smTran extends Model
{
    public $timestamps = false;

    protected $table = 'sm_tran';
    protected $primaryKey = 'noBatch';
    protected $fillable = [
        'noBatch', 'prdbln', 'prdthn', 'jurName', 'keterangan', 'amount', '	isPost', 'tgl_proses', 'userId', 'tgl_update'
    ];
    const CREATED_AT = 'tgl_update';

    function newData($data)
    {
        $save = smTran::insert($data);
        // $save->save();
        // if ($save->save()) {
        //     return $save;
        // }
    }

}
