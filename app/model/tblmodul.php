<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class tblmodul extends Model
{
    protected $table = "tblmodul";
    protected $primaryKey = 'idModul';
    protected $fillable = [
        'idModul', 'namaModul', 'isActive'
    ];
}
