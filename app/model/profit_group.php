<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class profit_group extends Model
{
    protected $table = 'profit_group';
    protected $fillable = [];

    public function profitGroup ()
    {
        return $this->hasOne('App\model\tblcoa', 'idGroup', 'idGroup');
    }
}
