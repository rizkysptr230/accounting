<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class tblmodul_list extends Model
{
    protected $table = "tblmodul_list";
    protected $fillable = [
        'idModul', 'idMenu', 'headMenu', 'subMenu', 'childMenu', 'nourut', 'tgl_proses', 'userId'
    ];

    public static function getModul_listById($id){
        $data=tblmodul_list::where('idModul',$id)
        ->where('subMenu',0)
        ->get();
        return $data;
    }
    public static function getModul_listById2($idModul, $idMenu){
        $data=tblmodul_list::where('idModul',$idModul)
        ->where('headMenu',$idMenu)
        ->where('subMenu',0)
        ->where('childMenu',0)
        ->get();
        return $data;
    }
}
