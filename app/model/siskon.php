<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class siskon extends Model
{
    public $timestamps = false;

    protected $table = 'siskon';
    protected $fillable = [];

    const CREATED_AT = 'tgl_proses';
}
