<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class dtTran extends Model
{
    public $timestamps = false;

    protected $table = 'dt_tran';
    protected $fillable = [];

    const CREATED_AT = 'tgl_validasi';

    public function coaName()
    {
        return $this->belongsTo('App\model\tblcoa', 'kodeCoa', 'kodeCoa');
    }

    public function profitGroup()
    {
        return $this->hasMany('App\model\profit_group');
    }


}
