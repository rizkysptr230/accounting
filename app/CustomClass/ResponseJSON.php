<?php
namespace App\CustomClass;

class ResponseJSON
{
    public $icon;
    public $title;
    public $text;
    public $code = 200;

    public function responseJson()
    {
        $response = [
            'message' => [
                'icon' => $this->icon,
                'title' => $this->title,
                'text' => $this->text
            ],
            'code' => $this->code
        ];

        return response($response, $this->code);
    }
}
