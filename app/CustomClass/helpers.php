<?php
namespace App\CustomClass;

use App\model\tblmenu;
use App\model\tblmodul_list;
use Session;
use DB;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\RequestException;


class helpers
{
    static function getModulList(){
        $data = tblmodul_list::getModul_listById(Auth::user()->idModul);
        return empty($data) ? null : $data;
    }
    static function getNameMenu($id){
        $data = tblmenu::getNameByIdMenu($id);
        return empty($data) ? null : $data;
    }
    static function getModulList2($idMenu){
        $data = tblmodul_list::getModul_listById2(Auth::user()->idModul,$idMenu);
        return empty($data) ? null : $data;
    }
    function bulan($nomor){
        $month = [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        ];
        return $month[$nomor];
    }

    static function api($method,$url, $param = null){
        $token = Session::get('token');
        $client = new \GuzzleHttp\Client();
        $body = [
            'query' => $param,
            'headers' => [
                'Authorization' => "Bearer {$token}"
                // 'Key-auth' => api::Key,
            ],
        ];
        try {
            //$response = $client->request($method, api::BaseAPI.$url, $body);
            $response = $client->request($method, $url, $body);
            $status = $response->getStatusCode(); 
            $contents =  $response->getBody()->getContents();
            $r = json_decode($contents);
            return $r;
        }catch(RequestException $e){
            $status = $e->getResponse()->getStatusCode(); 
            $content = $e->getResponse()->getBody()->getContents();
            return $e;
        }
    }
}