<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\model\tblcoa;
use App\model\smTran;
use App\model\dtTran;
class PostingJournalDetailController extends Controller
{
    public function index($noBatch)
    {
        $coaOption = tblcoa::select('kodeCoa', 'keterangan')
                            ->get();

        $dtTranData = dtTran::select('prdbln', 'prdthn', 'kodeCoa', 'keterangan', 'dk', 'amount', 'tgl_validasi')
                            ->where('noBatch', $noBatch)
                            ->get();

        $journalTitle = smTran::select('jurName', 'keterangan', 'amount', 'tgl_update')
                                ->where('noBatch', $noBatch)
                                ->first();

        $value = [];

        foreach ($dtTranData as $dtTranDatas) {
            $value [] = $dtTranDatas['amount'];
        }
        $totalPrice = array_sum($value);

        return view('posting-journals.details.index', compact('coaOption', 'journalTitle', 'noBatch', 'dtTranData', 'totalPrice'));
    }
}
