<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\model\dtTran;
use App\model\tblcoa;
use App\model\profit_group;

use DataTables;


class ProfitTableController extends Controller
{
    public function index(Request $request)
    {
        $month = $request->input('month');
        $year = $request->input('year');
        $group = $request->input('group');

        
        $getAllData = dtTran::where('prdbln', $month)
                            ->where('prdthn', $year)
                            ->when($group, function($query, $group) {
                                $query->whereHas('coaName', function($query) use ($group) {
                                    $query->where('idGroup', $group);
                                });
                            })
                            ->with([
                                'coaName', 
                            ])
                            ->get();

        $clearData = [];                            
        foreach ($getAllData as $data) {

            $clearData [$data->kodeCoa] = [
                'debit' => [],
                'kredit' => [],
                'keterangan' => $data['coaName']['keterangan'],
            ];

            if ($data->dk == 'D') {
                $clearData [$data->kodeCoa] ['debit'] [] = $data->amount;
            } else {
                $clearData [$data->kodeCoa] ['kredit'] [] = $data->amount;
            }
        }
        $AllData = [];

        foreach ($clearData as $cleanDataKey => $cleanData) {
            $AllData [$cleanDataKey] ['kodeCoa'] = $cleanDataKey;
            $AllData [$cleanDataKey] ['keterangan'] = $cleanData['keterangan'];

            $lastBalance = 0;
            // If have latest balance, we get here
            $checkLastBalance = dtTran::where('kodeCoa', $cleanDataKey)
                                        ->whereYear('prdthn', '=', $year)
                                        ->get();

            $AllData [$cleanDataKey] ['saldo'] = $lastBalance;

            // Calculatiing Debit and Kredit
            $debitValue = array_sum($cleanData['debit']);
            $kreditValue = array_sum($cleanData['kredit']);

            $AllData [$cleanDataKey] ['debit'] = $debitValue;
            $AllData [$cleanDataKey] ['kredit'] = $kreditValue;
            
            // Calculating Mutation
            $mutation = $debitValue + $kreditValue;
            $AllData [$cleanDataKey] ['mutasi'] = $mutation;
            
            // Calculating Latest Balance
            $latestBalance = $mutation + $lastBalance;
            $AllData [$cleanDataKey] ['saldoAkhir'] = $latestBalance;

        }

        $datatables =  DataTables::of($AllData)
                         ->addIndexColumn()
                         ->addColumn('mutasi', 'profit.datatables.mutation')
                         ->rawColumns([
                            'mutasi'
                         ])
                         ->make(true);// to json

        return $datatables;
    }
}
