<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\model\tblcoa;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;

class Coa extends Controller
{
    public function index()
    {
        $tblcoa = new tblcoa;
        return view('login.coa.index',[
            "data"=>$tblcoa->getAllData(),
        ]);
    }
    public function getCoa(Request $request)
    {
        $tblcoa = new tblcoa;
        return view('login.coa.edit',[
            "data"=>$tblcoa->getDataByKode($request->id),
            "id"=>$request->id
        ]);
    }
    public function putCoa(Request $request)
    {
        $tblcoa = new tblcoa;
        $data = array(
            'kodeCoa'=>$request->kodeCoa, 
            'nourut'=>$request->nourut, 
            'keterangan'=>$request->keterangan, 
            'isActive'=>$request->isActive
        );
        try{
            DB::beginTransaction();
            $tblcoa->updateDataByKode($request->id,$data);
        }catch (\Exception  $e) {
			DB::rollback();
		   	return redirect()->back()
		   	->with('debugcode','2')
            ->with('message','Error, Silahkan Hubungi CS '.$e);

		}catch (\Throwable  $e) {
			DB::rollback();
		   	return redirect()->back()
            ->with('debugcode','2')
            ->with('message','Error, Silahkan Hubungi CS');
        }
        DB::commit();
        return redirect()->route('login.getcoa',[$request->kodeCoa])
        ->with('debugcode','1')
        ->with('message','Data Berhasil Diperbarui');
    }
    public function newCoa()
    {
        $tblcoa = new tblcoa;
        return view('login.coa.add');
    }
    public function postCoa(Request $request)
    {
        $this::validate($request,[
            'kodeCoa'=> 'required|numeric|max:999999|regex:/^[\w-]*$/|unique:tblcoa,kodeCoa',
            'keterangan'=> 'required||regex:/^[\w-]*$/',
        ]); 
        $tblcoa = new tblcoa;
        $data = array(
            'kodeCoa'=>$request->kodeCoa, 
            'nourut'=>$request->nourut, 
            'keterangan'=>$request->keterangan, 
            'isActive'=>$request->isActive
        );
        try{
            DB::beginTransaction();
            $tblcoa->insertData($data);
        }catch (\Exception  $e) {
			DB::rollback();
		   	return redirect()->back()
		   	->with('debugcode','2')
            ->with('message','Error, Silahkan Hubungi CS '.$e);

		}catch (\Throwable  $e) {
			DB::rollback();
		   	return redirect()->back()
            ->with('debugcode','2')
            ->with('message','Error, Silahkan Hubungi CS');
        }
        DB::commit();
        return redirect()->route('login.getcoa',[$request->kodeCoa])
        ->with('debugcode','1')
        ->with('message','Data Berhasil Disimpan');
    }
}