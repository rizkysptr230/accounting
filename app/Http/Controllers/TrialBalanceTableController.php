<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\model\dtTran;
use App\model\tblcoa;

use DataTables;

class TrialBalanceTableController extends Controller
{
    public function index(Request $request)
    {
        $month = $request->input('month');
        $year = $request->input('year');

        
        $getAllDataInPeriod = dtTran::where('prdbln', $month)
                                    ->where('prdthn', $year)
                                    ->with([
                                        'coaName'
                                    ])
                                    ->get();

        $clearData = [];                            
        foreach ($getAllDataInPeriod as $dataPeriod) {

            $clearData [$dataPeriod->kodeCoa] = [
                'debit' => [],
                'kredit' => [],
                'keterangan' => $dataPeriod['coaName']['keterangan']
            ];

            if ($dataPeriod->dk == 'D') {
                $clearData [$dataPeriod->kodeCoa] ['debit'] [] = $dataPeriod->amount;
            } else {
                $clearData [$dataPeriod->kodeCoa] ['kredit'] [] = $dataPeriod->amount;
            }
        }
        
        $finalData = [];
        /**
         * cleanDataKey == Kode COA
         */
        foreach ($clearData as $cleanDataKey => $cleanData) {
            $finalData [$cleanDataKey] ['kodeCoa'] = $cleanDataKey;
            $finalData [$cleanDataKey] ['keterangan'] = $cleanData['keterangan'];

            $lastBalance = 0;
            // If have latest balance, we get here
            $checkLastBalance = dtTran::where('kodeCoa', $cleanDataKey)
                                        ->whereYear('prdthn', '<=', $year)
                                        ->get();

            $finalData [$cleanDataKey] ['saldo'] = $lastBalance;

            // Calculatiing Debit and Kredit
            $debitValue = array_sum($cleanData['debit']);
            $kreditValue = array_sum($cleanData['kredit']);

            $finalData [$cleanDataKey] ['debit'] = $debitValue;
            $finalData [$cleanDataKey] ['kredit'] = $kreditValue;
            
            // Calculating Mutation
            $mutation = $debitValue + $kreditValue;
            $finalData [$cleanDataKey] ['mutasi'] = $mutation;
            
            // Calculating Latest Balance
            $latestBalance = $mutation + $lastBalance;
            $finalData [$cleanDataKey] ['saldoAkhir'] = $latestBalance;

        }

        // $finalData = push([
        //     'kodeCoa' => $finalData['kodeCoa'],
        //     'keterangan' => $finalData['keterangan'],
        //     'saldo' => $finalData['saldo'],
        //     'debit' => $finalData['debit'],
        //     'kredit' => $finalData['kredit'],
        //     'saldoAkhir' => $finalData['saldoAkhir'],
        //     'mutasi' => $finalData['mutasi'],
        // ]);

        // dd($finalData);

        // $finalData = array(
        //     $finalData['kodeCoa'],
        //     $finalData['keterangan'],
        //     $finalData['saldo'],
        //     $finalData['debit'],
        //     $finalData['kredit'],
        //     $finalData['saldoAkhir'],
        //     $finalData['mutasi']
        // );
        
        // dd($finalData);


        $datatables =  DataTables::of($finalData)
                         ->addIndexColumn()
                        //  ->addColumn('kodeCoa', 'trial-balance.datatables.kodeCoa')
                        //  ->addColumn('keterangan', 'trial-balance.datatables.desc')
                         ->addColumn('saldo', 'trial-balance.datatables.balance')
                         ->addColumn('debit', 'trial-balance.datatables.debit')
                         ->addColumn('kredit', 'trial-balance.datatables.credit')
                         ->addColumn('saldoAkhir', 'trial-balance.datatables.endbalance')
                         ->addColumn('mutasi', 'trial-balance.datatables.mutation')
                         ->rawColumns([
                            'saldo', 'debit', 'kredit', 'saldoAkhir', 'mutasi'
                         ])
                         ->make(true);// to json

        return $datatables;
    }
}
