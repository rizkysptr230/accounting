<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\model\dtTran;

use DataTables;

class ManualJournalDetailTableController extends Controller
{
    public function index()
    {
        $getLatestJournalDetail = dtTran::where('kodeCoa', $kodeCoa);

        // dd($getLatestJournalDetail);

        $datatables =  DataTables::eloquent($getLatestJournalDetail)
                ->addIndexColumn()
                ->addColumn('kodeCoa', 'manual-journals.details.datatables.kodeCoa')
                ->addColumn('delete', 'manual-journals.details.datatables.delete')
                ->rawColumns([
                'kodeCoa', 'delete'
                ])
                ->make(true);// to json

        return $datatables;
    }
}
