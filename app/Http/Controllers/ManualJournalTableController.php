<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\model\smTran;

use DataTables;

class ManualJournalTableController extends Controller
{
    public function index()
    {
        $journals = smTran::where('isPost', '!=', 1);

        $datatables =  DataTables::eloquent($journals)
                         ->addIndexColumn()
                         ->addColumn('period', 'manual-journals.datatables.period')
                         ->addColumn('amount', 'manual-journals.datatables.amount')
                         ->addColumn('post', 'manual-journals.datatables.post')
                         ->addColumn('delete', 'manual-journals.datatables.delete')
                         ->rawColumns([
                            'period', 'post', 'delete', 'amount'
                         ])
                         ->make(true);// to json

        return $datatables;
    }

}
