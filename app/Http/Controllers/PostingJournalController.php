<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\model\smTran;

use Carbon\Carbon;

class PostingJournalController extends Controller
{
    public function index()
    {
        
        $postJournals = smTran::select('noBatch', 'prdbln', 'prdthn', 'jurName', 'keterangan', 'amount', 'isPost', 'tgl_proses')
                                ->where('isPost', '=', 1)
                                ->get();

        $posting = [];
        $no = 1;
        foreach ($postJournals as $postJournal) {
            $posting [] = [
                'no' => $no,
                'period' => Carbon::createFromIsoFormat('MM', $postJournal['prdbln'])->isoFormat('MMMM'). " - ". $postJournal['prdthn'] ,
                'date' => $postJournal['tgl_proses'],
                'name' => $postJournal['jurName'],
                'description' => $postJournal['keterangan'],
                'amount' => number_format($postJournal['amount'], 2),
                'batch' => $postJournal['noBatch']
            ];

            $no += 1;
        }


        return view('posting-journals.index', compact('posting'));
    }
}
