<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\model\smTran;
use App\model\siskon;
use App\model\dtTran;

use Carbon\Carbon;

class ManualJournalController extends Controller
{
    public function index()
    {
        return view('manual-journals.index');
    }

    public function store(Request $request)
    {
        // Get latest batch of journal
        $getLatestJournal = smTran::latest()->first();

        // Set default batch number
        $noBatch = 1;
        if ($getLatestJournal) {
            // Set batch number
            $noBatch = $getLatestJournal->noBatch + 1;
        }

        $period = $request->input('period');
        $year = date("Y", strtotime($period));
        $month = date("m", strtotime($period));

        $name = $request->input('name');
        $description = $request->input('description');
        $value = $request->input('value');
        $journal = new smTran;
        $data = [
            'noBatch' => $noBatch,
            'prdbln' => $month,
            'prdthn' => $year,
            'jurName' => $name,
            'keterangan' => $description,
            'amount' => $value,
            'isPost' => 0,
            'tgl_proses' => now(),
            'userId' => $value,
            'amount' => Auth::id(),
        ];
        // $journal->noBatch = $noBatch;
        // $journal->prdbln = $month;
        // $journal->prdthn = $year;
        // $journal->jurName = $name;
        // $journal->keterangan = $description;
        // $journal->amount = $value;
        // $journal->isPost = 0; //int
        // $journal->tgl_proses = now();
        // $journal->userId = Auth::id();

        try {
            DB::beginTransaction();
            // $journal->save();
            $journal->newData($data);
            DB::commit();

        } catch(\Exception $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        } catch(\Throwable $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);
        }

        $response = [
            'message' => [
                'icon' => 'success',
                'title' => 'Jurnal Tersimpan',
                'text' => 'Jurnal baru telah dibuat'
            ],
            'code' => 200
        ];

        return response($response, $response['code']);
    }

    private function responseJson()
    {
        $response = [
            'message' => [
                'icon' => $this->icon,
                'title' => $this->title,
                'text' => $this->text
            ],
            'code' => $this->code
        ];

        return response($response, $this->code);
    }


    public function update(Request $request, $noBatch)
    {
        $putDetailJournal = dtTran::select('amount')
                                    ->where('noBatch', $noBatch)
                                    ->get();

        $failed = [
            'message' => [
                'icon' => 'error',
                'title' => 'Saldo Tidak Balance',
                'text' => 'Gagal Posting'
            ],
            'code' => 200
        ];

        $total = [];

        foreach ($putDetailJournal as $putDetailJournals) {
            $total [] = $putDetailJournals['amount'];
        }
        $totalDetail = array_sum($total);

        if ($totalDetail > 0 || $totalDetail < 0 || $total == null) {
            return response($failed, $failed['code']);
        }


        try {
            DB::beginTransaction();

            smTran::where('noBatch', $noBatch)
            ->update(['isPost' => 1]);

            DB::commit();
        } catch(\Exception $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        } catch(\Throwable $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        }

        $response = [
            'message' => [
                'icon' => 'success',
                'title' => 'Posting',
                'text' => 'Jurnal Berhasil Diposting'
            ],
            'code' => 200
        ];

        return response($response, $response['code']);

    }

    public function destroy($noBatch)
    {
        try {
            DB::beginTransaction();

            smTran::where('noBatch', $noBatch)->delete();

            DB::commit();
        } catch(\Exception $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        } catch(\Throwable $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        }

        $response = [
            'message' => [
                'icon' => 'success',
                'title' => 'Jurnal Terhapus',
                'text' => 'Jurnal berhasil dihapus'
            ],
            'code' => 200
        ];

        return response($response, $response['code']);
    }
}
