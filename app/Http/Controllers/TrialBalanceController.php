<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\tblcoa;
use App\model\dtTran;

class TrialBalanceController extends Controller
{
    public function index()
    {   
        // Set Year
        $startYear = 2018;
        $year = [];

        for ($i=0; $i<25; $i++) {
            $year[] = $startYear + $i;
        }

        // Set Month
        $month = [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        ];

        $checkData = dtTran::latest()->first();

        $defaultData = [];
        if ($checkData) {
            $defaultData = [
                'year' => $checkData->prdthn, 
                'month' => $checkData->prdbln,
            ];

            $getAllDataInPeriod = dtTran::where('prdbln', $checkData->prdbln)
                                        ->where('prdthn', $checkData->prdthn)
                                        ->with([
                                            'coaName'
                                        ])
                                        ->get();
            $clearData = [];                            
            foreach ($getAllDataInPeriod as $dataPeriod) {

                $clearData [$dataPeriod->kodeCoa] = [
                    'debit' => [],
                    'kredit' => [],
                    'keterangan' => $dataPeriod['coaName']['keterangan']
                ];

                if ($dataPeriod->dk == 'D') {
                    $clearData [$dataPeriod->kodeCoa] ['debit'] [] = $dataPeriod->amount;
                } else {
                    $clearData [$dataPeriod->kodeCoa] ['kredit'] [] = $dataPeriod->amount;
                }
            }
        }


        return view('trial-balance.index', compact('year', 'month', 'defaultData'));
    }

    // public function filter(Request $request, dtTran $dtTran){
    //     $data = $dtTran->filter($request->bulan, $request->tahun);
    //     return view('trial-balance.filter',[
    //         'data'=>$data,
    //         'bulan'=>$request->bulan,
    //         'tahun'=>$request->tahun
    //     ]);
    // }
}
