<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Exception\RequestException;

use App\model\smtran;
use App\CustomClass\helpers;

class ApiController extends Controller
{
    public function dttran()
    {   
        
        $dtTran = helpers::api("GET","http://gfat.test/api/dtTran")->response->data;

        // dd($dtTran);
        
        return view('api.dttran', compact('dtTran'));
    }

    public function smtran()
    {   
        
        $smTran = helpers::api("GET","http://gfat.test/api/smTran")->response->data;

        // dd($smTran);
        
        return view('api.smtran', compact('smTran'));
    }
}
