<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\model\tblcoa;
use App\model\smTran;
use App\model\dtTran;

class ManualJournalDetailController extends Controller
{
    public function index($noBatch)
    {

        $coaOption = tblcoa::select('kodeCoa', 'keterangan')
                            ->where('nourut', '0')
                            ->get();

        $dtTranData = dtTran::select('id', 'prdbln', 'prdthn', 'kodeCoa', 'keterangan', 'dk', 'amount', 'tgl_validasi')
                            ->where('noBatch', $noBatch)
                            ->get();

        $journalTitle = smTran::select('jurName', 'keterangan', 'amount', 'tgl_update')
                                ->where('noBatch', $noBatch)
                                ->first();

        // total
        $value = [];

        foreach ($dtTranData as $dtTranDatas) {
            $value [] = $dtTranDatas['amount'];
        }
        $totalPrice = array_sum($value);

        return view('manual-journals.details.index', compact('coaOption', 'journalTitle', 'noBatch', 'dtTranData', 'totalPrice'));
    }

    public function store(Request $request ,$noBatch)
    {   

        $journalData = smTran::select('noBatch', 'prdbln', 'prdthn', 'keterangan', 'tgl_proses')
                                ->where('noBatch', $noBatch)
                                ->first();

        $coaCode = $request->input('coa-code');
        $description = $request->input('description');
        $debitOrCredit = $request->input('debit-or-credit');
        $value = $request->input('value');

        $journalNoUrutData = tblcoa::select('nourut')
                                ->where('kodeCoa', $coaCode)
                                ->first();

        if ($debitOrCredit == 'K') {
            $value = -($value);
        }


        $journalDetail = new dtTran;
        $journalDetail->noBatch = $journalData['noBatch'];
        $journalDetail->prdbln = $journalData['prdbln'];
        $journalDetail->prdthn = $journalData['prdthn'];
        $journalDetail->nourut = $journalNoUrutData['nourut'];
        $journalDetail->kodeCoa = $coaCode;
        $journalDetail->keterangan = $description;
        $journalDetail->dk = $debitOrCredit;
        $journalDetail->amount = $value;
        $journalDetail->tgl_proses = $journalData['tgl_proses'];
        $journalDetail->tgl_validasi = now();

        try {
            DB::beginTransaction();

            $journalDetail->save();

            DB::commit();
        } catch(\Exception $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        } catch(\Throwable $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);
        }

        $response = [
            'message' => [
                'icon' => 'success',
                'title' => 'Jurnal Tersimpan',
                'text' => 'Jurnal Detail telah dibuat'
            ],
            'code' => 200
        ];

        return response($response, $response['code']);
    
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            dtTran::where('id', $id)
                    ->delete();

            DB::commit();
        } catch(\Exception $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        } catch(\Throwable $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        }

        $response = [
            'message' => [
                'icon' => 'success',
                'title' => 'Jurnal Terhapus',
                'text' => 'Jurnal berhasil dihapus'
            ],
            'code' => 200
        ];

        return response($response, $response['code']);
    }
}
