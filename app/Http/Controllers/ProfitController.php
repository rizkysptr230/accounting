<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\dtTran;
use App\model\tblcoa;
use App\model\profit_group;

class ProfitController extends Controller
{
    public function index()
    {   
        //Set Profit Group
        $group = profit_group::select('idGroup', 'name')
                                    ->get();

        // Set Year
        $startYear = 2018;
        $year = [];

        for ($i=0; $i<25; $i++) {
            $year[] = $startYear + $i;
        }

        // Set Month
        $month = [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        ];

        $profitTitle = profit_group::select('name')
                                    ->where('idGroup')
                                    ->get();
                                    // dd($profitTitle);

        $checkData = dtTran::latest()->first();

        $defaultData = [];
        if ($checkData) {
            $defaultData = [
                'year' => $checkData->prdthn, 
                'month' => $checkData->prdbln,
            ];
        }

        return view('profit.index', compact('group', 'year', 'month', 'defaultData'));
    }
}
