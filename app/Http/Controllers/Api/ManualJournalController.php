<?php

namespace App\Http\Controllers\Api;
use App\model\smTran;
use App\Construct\TypeKaryawan;
use App\CustomClass\ResponseJSON;
use DB;
use Exception;
use App\Http\Requests\ManualJournalRequest;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ManualJournalController extends Controller
{
    public function store(ManualJournalRequest $request)
    {
        $responseJSON = new ResponseJSON;

        $type = 2;
        if($type == TypeKaryawan::Staff)
        {
            $gaji = 100000000;
        }elseif ($type == TypeKaryawan::Manager) {
            $gaji = 20000000;
        }else {
            $gaji = 0;
        }
        echo $gaji;
        die();
        // $validator = Validator::make($request->all(),
        // [
        // 'period' => 'required',
        // 'name' => 'required',
        // 'description' => 'required',
        // 'value' => 'required',
        // ]);

        // if ($validator->fails())
        // {
        //      return response()->json(['errors'=>$validator->errors()]);
        // }

        // Get latest batch of journal
        $getLatestJournal = smTran::latest()->first();

        // Set default batch number
        $noBatch = 1;
        if ($getLatestJournal) {
            // Set batch number
            $noBatch = $getLatestJournal->noBatch + 1;
        }

        $period = $request->input('period');
        $year = date("Y", strtotime($period));
        $month = date("m", strtotime($period));

        $name = $request->input('name');
        $description = $request->input('description');
        $value = $request->input('value');
        $journal = new smTran;
        $data = [
            'noBatch' => $noBatch,
            'prdbln' => $month,
            'prdthn' => $year,
            'jurName' => $name,
            'keterangan' => $description,
            'amount' => $value,
            'isPost' => 0,
            'tgl_proses' => now(),
            'userId' => $value,
            'amount' => 1,
        ];
        // $journal->noBatch = $noBatch;
        // $journal->prdbln = $month;
        // $journal->prdthn = $year;
        // $journal->jurName = $name;
        // $journal->keterangan = $description;
        // $journal->amount = $value;
        // $journal->isPost = 0; //int
        // $journal->tgl_proses = now();
        // $journal->userId = Auth::id();

        try {
            DB::beginTransaction();
            // $journal->save();
            $journal->newData($data);
            DB::commit();
            $responseJSON->icon = 'success';
            $responseJSON->text = 'Data Berhasil Disimpan';
            $responseJSON->title = 'Input Data';

            return $responseJSON->responseJson();

        } catch(Exception $ex) {
            DB::rollBack();
            $responseJSON->icon = 'error';
            $responseJSON->text = 'Data Error';
            $responseJSON->title = 'Input Data';
            $responseJSON->code = 500;

            return $responseJSON->responseJson();
        }
    }
}
