<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\model\dtTran;
use App\model\tblcoa;
use App\model\neraca_group;

use DataTables;

class NeracaTableController extends Controller
{
    public function index(Request $request)
    {
        $month = $request->input('month');
        $year = $request->input('year');
        $group = $request->input('group');

        
        $getAllDataInPeriod = dtTran::where('prdbln', $month)
                                    ->where('prdthn', $year)
                                    ->when($group, function($query, $group) {
                                        $query->whereHas('coaName', function($query) use ($group) {
                                            $query->where('idGroup', $group);
                                        });
                                    })
                                    ->with([
                                        'coaName'
                                    ])
                                    ->get();
        $clearData = [];                            
        foreach ($getAllDataInPeriod as $dataPeriod) {

            $clearData [$dataPeriod->kodeCoa] = [
                'debit' => [],
                'kredit' => [],
                'keterangan' => $dataPeriod['coaName']['keterangan']
            ];

            if ($dataPeriod->dk == 'D') {
                $clearData [$dataPeriod->kodeCoa] ['debit'] [] = $dataPeriod->amount;
            } else {
                $clearData [$dataPeriod->kodeCoa] ['kredit'] [] = $dataPeriod->amount;
            }
        }
        
        $AllData = [];

        foreach ($clearData as $cleanDataKey => $cleanData) {
            $AllData [$cleanDataKey] ['kodeCoa'] = $cleanDataKey;
            $AllData [$cleanDataKey] ['keterangan'] = $cleanData['keterangan'];

            $lastBalance = 0;
            // If have latest balance, we get here
            $checkLastBalance = dtTran::where('kodeCoa', $cleanDataKey)
                                        ->whereYear('prdthn', '=', $year)
                                        ->get();

            $AllData [$cleanDataKey] ['saldo'] = $lastBalance;

            // Calculatiing Debit and Kredit
            $debitValue = array_sum($cleanData['debit']);
            $kreditValue = array_sum($cleanData['kredit']);

            $AllData [$cleanDataKey] ['debit'] = $debitValue;
            $AllData [$cleanDataKey] ['kredit'] = $kreditValue;
            
            // Calculating Mutation
            $mutation = $debitValue + $kreditValue;
            $AllData [$cleanDataKey] ['mutasi'] = $mutation;
            
            // Calculating Latest Balance
            $latestBalance = $mutation + $lastBalance;
            $AllData [$cleanDataKey] ['saldoAkhir'] = $latestBalance;

        }

        $datatables =  DataTables::of($AllData)
                         ->addIndexColumn()
                        //  ->addColumn('kodeCoa', 'neraca.datatables.kodeCoa')
                        //  ->addColumn('keterangan', 'neraca.datatables.desc')
                         ->addColumn('mutasi', 'neraca.datatables.mtd')
                         ->addColumn('saldoAkhir', 'neraca.datatables.ytd')
                         ->rawColumns([
                            'saldoAkhir', 'mutasi'
                         ])
                         ->make(true);// to json

        return $datatables;
    }
}
