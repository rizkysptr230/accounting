<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\tblcoa;

class CoaCodeListController extends Controller
{
    public function index()
    {
        $coa = tblcoa::select('nourut as id', DB::raw('CONCAT(kodeCoa, " - ", keterangan) as text'))
                      ->get();

        return $coa;
    }
}
