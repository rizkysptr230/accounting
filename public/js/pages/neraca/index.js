/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/pages/neraca/index.js":
/*!********************************************!*\
  !*** ./resources/js/pages/neraca/index.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  $('#filter-neraca-form').submit(function (e) {
    $(e.preventDefault());
    $('#neraca-table').DataTable().ajax.reload();
  });
  $('#neraca-table').DataTable({
    searching: false,
    paging: false,
    info: false,
    processing: true,
    responsive: true,
    serverSide: true,
    ajax: {
      url: '/neraca-tables',
      method: 'get',
      data: function data(_data) {
        _data.group = $('select[name=filter-group]').val();
        _data.month = $('select[name=filter-month]').val();
        _data.year = $('select[name=filter-year]').val();
      }
    },
    columns: [{
      data: 'DT_RowIndex',
      searchable: false,
      orderable: false
    }, {
      data: 'kodeCoa',
      name: 'kodeCoa'
    }, {
      data: 'keterangan',
      name: 'keterangan'
    }, {
      data: 'saldoAkhir',
      name: 'saldoAkhir',
      className: 'sum',
      render: $.fn.dataTable.render.number('.', '.')
    }, {
      data: 'mutasi',
      name: 'mutasi',
      className: 'sum',
      render: $.fn.dataTable.render.number('.', '.')
    }],
    'footerCallback': function footerCallback() {
      var api = this.api();
      api.columns('.sum').every(function () {
        var sum = this.data().reduce(function (a, b) {
          var x = parseFloat(a) || 0;
          var y = parseFloat(b) || 0;
          return x + y;
        }, 0); // alert(sum)

        if (sum < 0) {
          $(this.footer()).html(sum).mask('-' + '000.000.000.000.000', sum);
        }
      });
    }
  });
});

/***/ }),

/***/ 7:
/*!**************************************************!*\
  !*** multi ./resources/js/pages/neraca/index.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\accounting\resources\js\pages\neraca\index.js */"./resources/js/pages/neraca/index.js");


/***/ })

/******/ });