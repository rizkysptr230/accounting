const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/pages/coa/index.js', 'public/js/pages/coa')
    .js('resources/js/pages/manual-journals/index.js', 'public/js/pages/manual-journals')
    .js('resources/js/pages/manual-journals/details/index.js', 'public/js/pages/manual-journals/details')
    .js('resources/js/pages/posting-journals/index.js', 'public/js/pages/posting-journals')
    .js('resources/js/pages/posting-journals/details/index.js', 'public/js/pages/posting-journals/details')
    .js('resources/js/pages/trial-balance/index.js', 'public/js/pages/trial-balance')
    .js('resources/js/pages/neraca/index.js', 'public/js/pages/neraca')
    .js('resources/js/pages/profit/index.js', 'public/js/pages/profit')
    .js('resources/js/pages/auth/login.js', 'public/js/pages/auth')
    .sass('resources/sass/app.scss', 'public/css');
